//
//  Expenses.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-27.
//

import UIKit

class Expenses: UIViewController, UITableViewDelegate, UITableViewDataSource , addExpenseProtocol {
    func refresh() {
        makeGetCall()
    }
    var refreshControl = UIRefreshControl()

    @IBOutlet weak var expenseButtonView: CurvedView!
    
    var jobData : Datum?
    var expenseData : ExpensesJSON?
    @IBOutlet weak var expenseTableView: UITableView!
    @IBOutlet weak var totalAdvance: UILabel!
    @IBOutlet weak var totalCash: UILabel!
    @IBOutlet weak var balance: UILabel!
    @IBOutlet weak var retunrPay: UILabel!
    @IBOutlet weak var totalCommission: UILabel!
    @IBOutlet weak var youWillGetLabel: UILabel!
    
    var totalExpense : Double = 0.0
    var editExpense : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
     //   payLabel.text = ""
        expenseTableView.delegate = self
        expenseTableView.dataSource = self
        expenseTableView.backgroundView = nil
        expenseTableView.allowsSelection = false
        expenseTableView.isOpaque = false
        expenseTableView.backgroundColor = #colorLiteral(red: 0.956779182, green: 0.9569163918, blue: 0.956749022, alpha: 1)
        expenseTableView.register(UINib(nibName: "ExpenseListCell", bundle: nil) , forCellReuseIdentifier: "ExpenseListCell")
        makeGetCall()
        self.title = "\(jobData?.jobNumber ?? "") - EXPENSE"
      
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
           refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        expenseTableView.addSubview(refreshControl)

    }
    @objc func refresh(_ sender: AnyObject) {
        makeGetCall()
        refreshControl.endRefreshing()

        //makePostCallDelivered()
       // Code to refresh table view
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if expenseData?.expense?.count ?? 0 == 0 {
              self.expenseTableView.setEmptyMessage("No Expenses Found")
          } else {
            self.expenseTableView.restore()
          }
        return  expenseData?.expense?.count ?? 0
    }
    
    @IBAction func addExpenseClicked(_ sender: Any) {
        performSegue(withIdentifier: "toAddExpense", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddExpense"
        {
            let vc = segue.destination as! AddExpense
            vc.jobData = jobData
            vc.delegate = self
            if editExpense != nil
            {
                vc.editData = expenseData?.expense?[editExpense!]
                editExpense = nil
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = expenseTableView.dequeueReusableCell(withIdentifier: "ExpenseListCell") as! ExpenseListCell
      if  let x = expenseData?.expense?[indexPath.row]
      {
        let d = convertDateFormater(x.expenseModifyDate!)
        cell.date.text = d.0
        cell.time.text = d.1
        if x.expenseStatus == ""
        {
            cell.name.text = "PENDING"

        }
        else
        {
        cell.name.text = x.expenseStatus ?? ""
        }
        cell.type.text = x.expenseType ?? ""
        cell.circleVIew.layer.cornerRadius = cell.circleVIew.frame.width / 2
        var y = Double(x.expenseCash ?? "0.0")
        var s = String(format: "%.2f", y ?? 0.0)
        cell.mode.text = "CASH: MYR \(s)"
         y = Double(x.expenseCard ?? "0.0")
         s = String(format: "%.2f", y ?? 0.0)
        cell.amount.text = "CARD: MYR \(s)"
        cell.remarks.text = x.expenseRemark ?? ""
        if x.expenseImage != nil
        {
            let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)expense/\(x.expenseImage!)").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            print(urlStr as Any)
            let url = URL(string: urlStr!)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                    cell.expenseImage.image = UIImage(data: data!)
                   // logoImageVIew.image = UIImage(data: data!)
                  //  logoImageVIew.contentMode = .scaleAspectFit
                    
                  
                }
            }
        }
        if x.expenseType != nil
        {
            switch x.expenseType! {
            case "AUTO PASS SINGAPORE":
                cell.expenseIcon.image = UIImage(named: "singapore")
                cell.viewForBackground.applyGradient(color: .yellow)

            case "BAYARAN GUDANG":
                cell.expenseIcon.image = UIImage(named: "warehouse")
                cell.viewForBackground.applyGradient(color: .blue)
            case "BAYARAN PARKING":
                cell.expenseIcon.image = UIImage(named: "parking")
                cell.viewForBackground.applyGradient(color: .yellow)

            case "DGC":
                cell.expenseIcon.image = UIImage(named: "expense_autopass")
                cell.viewForBackground.applyGradient(color: .systemBlue)

            case "DIESEL/PETROL":
                cell.expenseIcon.image = UIImage(named: "expense_fuel")
              cell.viewForBackground.applyGradient(color: .systemBlue)

            case "FORKLIF":
                cell.expenseIcon.image = UIImage(named: "expense_forklift")
                cell.viewForBackground.applyGradient(color: .green)

            case "KEROSAKAN":
                cell.expenseIcon.image = UIImage(named: "cargo_damage")
                cell.viewForBackground.applyGradient(color: .blue)

            case "KREN":
                cell.expenseIcon.image = UIImage(named: "expense_levy")
                cell.viewForBackground.applyGradient(color: .systemRed)

            case "LEVI":
                cell.expenseIcon.image = UIImage(named: "expense_levy")
                cell.viewForBackground.applyGradient(color: .purple)

            case "PAS KLIA":
                cell.expenseIcon.image = UIImage(named: "levipass")
                cell.viewForBackground.applyGradient(color: .systemRed)

            case "PENDAHULUAN":
                cell.expenseIcon.image = UIImage(named: "expense_autopass")
                cell.viewForBackground.applyGradient(color: .systemBlue)

            case "TOL":
                cell.expenseIcon.image = UIImage(named: "toll")
                cell.viewForBackground.applyGradient(color: .black)

            default:
                print("unknown")
            }
        }

      }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        expenseTableView.deselectRow(at: indexPath, animated: false)
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
         {
        let x = expenseData?.expense?[indexPath.row]
        if x?.expenseStatus != "VERIFIED"
        {
       //  action.backgroundColor = .yellow
         let actionApply = UIContextualAction(style: .normal, title:  "Update", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                 print("Apply")
             success(true)

            self.editExpense = indexPath.row
            self.performSegue(withIdentifier: "toAddExpense", sender: nil)

                             
             })
            let actionDelete = UIContextualAction(style: .normal, title:  "Delete", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                    print("delete")
                success(true)

                self.deleteExpense(expenseid: (x?.expenseID)!)
                                
                })

     actionApply.backgroundColor = .green
            actionDelete.backgroundColor = .systemRed
   //  actionApply.image = UIImage(systemName: "hand.tap.fill")

             return UISwipeActionsConfiguration(actions: [actionApply , actionDelete])
        }
        else
        {
            return nil
        }
         }
    func deleteExpense(expenseid : String) {
      //  jobid primary id
        print("hh")
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(ConstantsUsedInProject.baseUrl)api/deleteExpense/\(expenseid)")! as URL)
        request.httpMethod = "GET"
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            do {

                let responseJ = try? decoder.decode(DeleteExpenseJSON.self, from: data!)
                print(responseJ as Any)

                let code_str = responseJ?.code
                DispatchQueue.main.async { [self] in
                    if code_str == 200 {
                        print("success")
                       // self.trackData = responseJ
                      makeGetCall()
                        let alert = UIAlertController(title: "Expense", message: "\(responseJ?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }else if code_str == 201  {
                       
                        let alert = UIAlertController(title: "Expense", message: "\(responseJ?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        task.resume()
    }
    func makeGetCall() {
      //  jobid primary id
        print("hh")
        _ = UserDefaults.standard.string(forKey: "id")
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(ConstantsUsedInProject.baseUrl)expense/\(jobData!.taFormID!)/\(jobData!.jobIdPrimary!)")! as URL)
     //   print("\(ConstantsUsedInProject.baseUrl)expense/\(jobData!.jobID!)/\(jobData!.jobIdPrimary!)")
        request.httpMethod = "GET"
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            do {

                let responseJ = try? decoder.decode(ExpensesJSON.self, from: data!)
                print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                let code_str = responseJ?.code
                DispatchQueue.main.async { [self] in
                    if code_str == 200 {
                        print("success")
                       // self.trackData = responseJ
                        self.expenseData = responseJ
                        totalExpense = 0.0
                        self.expenseTableView.reloadData()
                        for i in expenseData!.expense! {
                            let y = Double(i.expenseCash! )
                            totalExpense = y! + totalExpense
                            let c = Double(i.expenseCard! )
                            totalExpense = c! + totalExpense

                        }
                       // let z = String(format: "%.2f", totalExpense)
                       // expense.text = ":MYR \(z)"
                        totalAdvance.text = ":  \(expenseData?.expensereport?.totalAdvance ?? "0.00") MYR"
                        totalCash.text = ":  \(expenseData?.expensereport?.sumOfCashExpense ?? "0.00") MYR"
                        balance.text = ":  \(expenseData?.expensereport?.totalBalance ?? "0.00") MYR"
                        totalCommission.text = ":  \(expenseData?.expensereport?.totalCommision ?? "0.00") MYR"
                        retunrPay.text = ":  \(expenseData?.expensereport?.totalGivenbydriver ?? "0.00") MYR"
                        youWillGetLabel.text = ":  \(expenseData?.expensereport?.totalPaytodriver ?? "0.00") MYR"
                        print("operationsttus")
                        if expenseData?.expensereport?.operationStatus?.uppercased() == "VERIFIED"
                        {
                            expenseButtonView.isHidden = true
                        }
                        let x = check(modifyData: jobData!.modifydate!)
                        if x == false
                        {
                            expenseButtonView.isHidden = true

                        }
                        
                    }else   {
                        self.expenseData = responseJ
                        if expenseData?.expensereport?.operationStatus?.uppercased() == "VERIFIED"
                        {
                            expenseButtonView.isHidden = true
                        }
                        let x = check(modifyData: jobData!.modifydate!)
                        if x == false
                        {
                            expenseButtonView.isHidden = true

                        }

                        totalAdvance.text = ":  \(expenseData?.expensereport?.totalAdvance ?? "0.00") MYR"
                        totalCash.text = ":  \(expenseData?.expensereport?.sumOfCashExpense ?? "0.00") MYR"
                        balance.text = ":  \(expenseData?.expensereport?.totalBalance ?? "0.00") MYR"
                        totalCommission.text = ":  \(expenseData?.expensereport?.totalCommision ?? "0.00") MYR"
                        retunrPay.text = ":  \(expenseData?.expensereport?.totalGivenbydriver ?? "0.00") MYR"
                        youWillGetLabel.text = ":  \(expenseData?.expensereport?.totalPaytodriver ?? "0.00") MYR"
                        self.expenseData = responseJ
                        totalExpense = 0.0
                        self.expenseTableView.reloadData()
                        let alert = UIAlertController(title: "Expense", message: "\(responseJ?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        //self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        task.resume()
    }
    func check(modifyData : String) -> Bool
    {
        print(modifyData)
        var result = false
        let formatterForTime = DateFormatter()
        let date = Date()
        formatterForTime.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let passDate = formatterForTime.date(from: modifyData)!
        let currentDateF = formatterForTime.string(from: date as Date)
        let curr = formatterForTime.date(from: currentDateF) // curretn time
        let passPlusGrace = passDate.addingTimeInterval(60*60*24) // pass plus 15 grace
        print(formatterForTime.string(from: passPlusGrace))
        print(currentDateF)
       
        
        if curr! < passPlusGrace
        {
           result = true
        }
        else
        {
result = false
        }
        return result
    }
    func convertDateFormater(_ date: String) -> (String , String)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "h:mm a"
            let t = dateFormatter.string(from: date!)

            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  (dateFormatter.string(from: date!) , t)

        }
}

extension UIView
{
    func applyGradient(color : UIColor )
    {
        let gradient = CAGradientLayer()

        gradient.frame = self.bounds
        gradient.colors = [color.cgColor, UIColor.black.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 1)
        gradient.endPoint = CGPoint.zero
        self.layer.insertSublayer(gradient, at: 0)
    }
}
