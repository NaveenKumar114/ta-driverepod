//
//  JobDetails.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-23.
//

import UIKit
import MapKit
import Alamofire
import CoreLocation
import SystemConfiguration
import CoreTelephony
class JobDetails: UIViewController , CLLocationManagerDelegate, signatureDelegate {
    func signDone(signImage: UIImage, empName: String, empId: String) {
        toSignImage.image = signImage
        signatureImage = signImage
        self.signEmpid = empId
        self.signEmpName = empName
        fromDepartureButton(departureOrDeliveryBUtton as Any)

    }
    
   
    
    
    var address = ""
    var delegate : jobFinishedDelegate?
    var locationManager: CLLocationManager?
    var currentLocation : CLLocation?
    var flag = 1
    var previousLocation : CLLocation?
    @IBOutlet weak var departureOrDeliveryBUtton: UIButton!
    var departureFlag = false //to know if driver is departured and in strasnit
    var jobDoneFlag = false // if the job is done
    @IBOutlet weak var fromButtonsVIew: UIView!
    @IBOutlet weak var toButtonsView: UIView!
    @IBOutlet weak var toattachIamge: UIImageView!
    @IBOutlet weak var toSignImage: UIImageView!
    @IBOutlet weak var expenseVIew: CurvedView!
  
    @IBOutlet weak var activeView: ElevateAndCorner!
   
    @IBOutlet weak var activeJobNUmber: UILabel!
    @IBOutlet weak var activeStatus: UILabel!
    @IBOutlet weak var activeTaNumber: UILabel!
   
    @IBOutlet weak var remark: UILabel!
    @IBOutlet weak var viewWithIconFOrRound: UIView!
    @IBOutlet weak var activeImageview: UIImageView!
    @IBOutlet weak var activeButton: CurvedView!
    var activeAttachImage : UIImage?
    var signEmpid : String?
    var signEmpName : String?
   
    var signatureImage : UIImage?
    var jobData : Datum?
    
    @IBOutlet weak var payTODriverTExt: UILabel!
    @IBOutlet weak var commisiionText: UILabel!
    @IBOutlet weak var payTODriver: UILabel!
    @IBOutlet weak var fromName: UILabel!
    @IBOutlet weak var fromLocation: UILabel!
    @IBOutlet weak var toName: UILabel!
    @IBOutlet weak var toLocation: UILabel!
    @IBOutlet weak var truckType: UILabel!
    @IBOutlet weak var truckNumber: UILabel!
    @IBOutlet weak var commission: UILabel!
    @IBOutlet weak var date: UILabel!
    let child = SpinnerViewController()

    var isCamera = false //to see if the selected image is camera or gallery
    @IBOutlet weak var taNumber: UILabel!
    func createSpinnerView() {
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func removeSpinnerView()
    {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let vendor = UserDefaults.standard.string(forKey: userdefaultsKey.vendorOptionDriver)?.uppercased()
        if vendor == "OTHER"
        {
            expenseVIew.isHidden = true
        }
        if let x = jobData
        {
            if x.jobStatus == "PICKED UP"
            {
                jobDoneFlag = true
             //   fromIsActive()

            }

            if x.jobStatus == "DEPARTURE" || x.jobStatus == "IN TRANSIT"
            {
                departureFlag = true
                
            }
            else
            {
                departureFlag = false
                print("from")
            }
            if departureFlag
            {
                isInTransit()
                //toIsActive()
            }
            else
            {
                isCollecting()
                //fromIsActive()

            }
            labelsSetUp()

            
        }
        
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestAlwaysAuthorization()
        //locationManager?.allowsBackgroundLocationUpdates = true
        locationManager?.requestLocation()
        locationManager?.startUpdatingLocation()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (expensePressed(sender:)))
        expenseVIew.addGestureRecognizer(tapGesture)
        _ = UITapGestureRecognizer(target: self, action: #selector (mapPressed(sender:)))
        viewWithIconFOrRound.layer.cornerRadius = viewWithIconFOrRound.frame.height / 2
        self.navigationController?.isNavigationBarHidden = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    locationManager?.requestAlwaysAuthorization()

            case .authorizedAlways, .authorizedWhenInUse:
                    print("Access")
                @unknown default:
                break
            }
            } else {
                let alert = UIAlertController(title: "Expense", message: "Please turn on location", preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)        }
        if  locationManager?.location?.coordinate.latitude != nil
        {
            _ = String((locationManager?.location?.coordinate.latitude)!)
            _ = String((locationManager?.location?.coordinate.longitude)!)
            let cl = CLLocation(latitude: (locationManager?.location?.coordinate.latitude)!, longitude: (locationManager?.location?.coordinate.longitude)!)
            getAdressName(coords: (cl))

        }

        //viewWithMap.addGestureRecognizer(tapGesture2)
    }
    @objc func expensePressed(sender: UITapGestureRecognizer)
    {
        performSegue(withIdentifier: "toExpense", sender: nil)
        //performSegue(withIdentifier: "toMapTrack", sender: nil)
    }
    
    @objc func mapPressed(sender: UITapGestureRecognizer)
    {
        //performSegue(withIdentifier: "toExpense", sender: nil)
        performSegue(withIdentifier: "toMapTrack", sender: nil)
    }
    
    @IBAction func mapButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "toMapTrack", sender: nil)

    }
    @IBAction func trackView(_ sender: Any) {
        performSegue(withIdentifier: "toTrack", sender: nil)
    }
    @IBAction func expenseView(_ sender: Any) {
        performSegue(withIdentifier: "toExpense", sender: nil)

    }
    func isCollecting()
    {
        toButtonsView.isHidden = true
        fromButtonsVIew.isHidden = false

        
    }
    func isInTransit()
    {
        toButtonsView.isHidden = false
        fromButtonsVIew.isHidden = true
        departureOrDeliveryBUtton.setTitle("DELIVERED", for: .normal)
    }
    func labelsSetUp()
    {
        let x = jobData!
        activeJobNUmber.text = x.jobNumber
        if x.jobStatus == "ACCEPTED"
        {
            activeStatus.text = "COLLECTING"
        }
        else
        {
            activeStatus.text = x.jobStatus
            
        }
        var y = Double(x.totalPaytodriver ?? "0.0")
        var s = String(format: "%.2f", y ?? 0.0)

        payTODriver.text = "MYR \(s)"
         y = Double(x.jobCommision ?? "0.0")
         s = String(format: "%.2f", y ?? 0.0)

        commission.text = "MYR \(s)"
        fromName.text = x.jobFrom?.uppercased()
        toName.text = x.jobTo?.uppercased()
        
      //  toLocation.text = x.billtoLocationaddress
        //fromLocation.text = x.consigneeLocationaddress
        truckType.text = x.jobChargeType
        truckNumber.text = x.truckNumber
       // companyNmae.text = x.jobClientName
        taNumber.text = x.taTadynamicNumber
        date.text = convertDateFormaterForMonthNAme((x.jobDate!))
        fromName.text = x.shipperName?.uppercased()
        toName.text = x.consigneeName?.uppercased()
        fromLocation.text = x.shipperLocationaddress
        toLocation.text = x.consigneeLocationaddress
        remark.text = x.jobRemark ?? ""
        if x.jobStatus == "INCOMPLETE"
        {
            toButtonsView.isHidden = true
            fromButtonsVIew.isHidden = true
            departureOrDeliveryBUtton.isHidden = true
        }
        let vendor = UserDefaults.standard.string(forKey: userdefaultsKey.vendorOptionDriver)?.uppercased()
        if vendor == "OTHER"
        {
            let y = Double(x.jobCommision ?? "0.0")
            let s = String(format: "%.2f", y ?? 0.0)
            payTODriver.text = "MYR \(s)"
            commission.text = ""
            commisiionText.text = "Rental"
            payTODriverTExt.text = ""
        }
        
    }
    func getAdressName(coords: CLLocation) {

        CLGeocoder().reverseGeocodeLocation(coords) { (placemark, error) in
                if error != nil {
                    print("Hay un error")
                } else {

                    let place = placemark! as [CLPlacemark]
                    if place.count > 0 {
                        let place = placemark![0]
                        var adressString : String = ""
                        if place.thoroughfare != nil {
                            adressString = adressString + place.thoroughfare! + ", "
                        }
                        if place.subThoroughfare != nil {
                            adressString = adressString + place.subThoroughfare! + ", "
                        }
                        if place.locality != nil {
                            adressString = adressString + place.locality! + " - "
                        }
                        if place.postalCode != nil {
                            adressString = adressString + place.postalCode! + ", "
                        }
                        if place.subAdministrativeArea != nil {
                            adressString = adressString + place.subAdministrativeArea! + " - "
                        }
                        if place.country != nil {
                            adressString = adressString + place.country!
                        }
                        self.address = adressString
                        print(self.address)
                    }
                }
            }
      }
    func convertDateFormaterForMonthNAme(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MMM-yyyy"
            return  dateFormatter.string(from: date!)

        }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  dateFormatter.string(from: date!)

        }

  
    @objc func toImage()
    {
        let chooseImageActionMenu = UIAlertController(title: "Choose an option", message: nil, preferredStyle: .actionSheet)
        let galleryButton = UIAlertAction(title: "Gallery", style: .default) { (_) in
            self.openGallery()
        }
        let cameraButton = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.openCamera()
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        chooseImageActionMenu.addAction(galleryButton)
        chooseImageActionMenu.addAction(cameraButton)
        chooseImageActionMenu.addAction(cancelButton)
        self.present(chooseImageActionMenu, animated: true, completion: nil)
    }

    @IBAction func fromPhotoBUtton(_ sender: Any) {
      
      
        toImage()
    }
    @IBAction func toPhotoButton(_ sender: Any) {
        toImage()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSign"
        {
            let vc = segue.destination as! DriverSignature
            vc.delegate = self
        }
        if segue.identifier == "toTrack"
        {
            let vc = segue.destination as! TrackJob
            vc.jobData = jobData!
        }
        if segue.identifier == "toExpense"
        {
            let vc = segue.destination as! Expenses
            vc.jobData = jobData!
        }
        if segue.identifier == "toMapTrack"
        {
            let vc = segue.destination as! TrackInMap
            vc.jobData = jobData!
        }
        
    }
    func getConnectionType() -> String {
        guard let reachability = SCNetworkReachabilityCreateWithName(kCFAllocatorDefault, "www.google.com") else {
            return "NO INTERNET"
        }
        
        var flags = SCNetworkReachabilityFlags()
        SCNetworkReachabilityGetFlags(reachability, &flags)
        
        let isReachable = flags.contains(.reachable)
        let isWWAN = flags.contains(.isWWAN)
        
        if isReachable {
            if isWWAN {
                let networkInfo = CTTelephonyNetworkInfo()
                let carrierType = networkInfo.serviceCurrentRadioAccessTechnology
                
                guard let carrierTypeName = carrierType?.first?.value else {
                    return "UNKNOWN"
                }
                
                switch carrierTypeName {
                case CTRadioAccessTechnologyGPRS, CTRadioAccessTechnologyEdge, CTRadioAccessTechnologyCDMA1x:
                    return "2G"
                case CTRadioAccessTechnologyLTE:
                    return "4G"
                default:
                    return "3G"
                }
            } else {
                return "WIFI"
            }
        } else {
            return "NO INTERNET"
        }
    }
    @IBAction func fromDepartureButton(_ sender: Any) {

        if departureFlag
        {
            if activeAttachImage == nil || signatureImage == nil
            {
                let alert = UIAlertController(title: "Job", message: "Please attach a image and sign", preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                //self.present(alert, animated: true, completion: nil)
            }
            else
            {
                prepareForUpload(status: "PICKED UP")
            }
        }
        else
        {
        if activeAttachImage == nil
        {
            let alert = UIAlertController(title: "Job", message: "Please attach a image", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
           // self.present(alert, animated: true, completion: nil)
        }
        else
        {
            prepareForUpload(status: "PICKED UP")
        }
        }
    }
    func prepareForUpload(status : String)
    {
        UIDevice.current.isBatteryMonitoringEnabled = true
        print(UIDevice.current.isBatteryMonitoringEnabled)
        var level = UIDevice.current.batteryLevel
        print(level*100)
        if level == -1.0
        {
            level = 0.00
        }
        let version = UIDevice.current.systemVersion
        print(version)
        let name = UIDevice.current.name
        print(name)
        let model = UIDevice.current.model
        print(model)
        let state = UIDevice.current.batteryState
        var batteryType = "discharging"
        switch state {
        case .charging:
            print("charging")
        case .full:
            batteryType = "full"
            
        case .unplugged:
            batteryType = "discharging"
            
        default:
            print("else")
        }
        print(getConnectionType())
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        
        // Get carrier name
        var carrierName = carrier?.carrierName
        if carrierName == nil
        {
            carrierName = ""
        }
       
        let id = UserDefaults.standard.string(forKey: "id")
        print("hhh")
        let x = jobData!
        var lat = ""
        var long = ""
        var speed = ""
        var speedAccur = ""
        var accur = ""
        var bearing = ""
        var bearingDegree = ""
        if currentLocation != nil{
            lat = String((currentLocation?.coordinate.latitude)!)
            long = String((currentLocation?.coordinate.longitude)!)
            speed = String((currentLocation?.speed)!)
            speedAccur = String((currentLocation?.speedAccuracy)!)
            accur = String((currentLocation?.horizontalAccuracy)!)
            if previousLocation != nil
            {
                let y = getBearingBetweenTwoPoints1(point1: currentLocation!, point2: previousLocation!)
                bearingDegree = String(y.0)
                bearing = String(y.1)
                print(bearing)
                
            }

        }
        let defaults = UserDefaults.standard

        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    //    formatter.timeZone = TimeZone(abbreviation: "UTC")
        let defaultTimeZoneStr = formatter.string(from: date as Date)
        var compressionAmount : CGFloat = 0.3
        if isCamera{
            compressionAmount = 0.1
        }
        createSpinnerView()
        if departureFlag
        {
            let json: [String: Any] = ["sign_emp_id" : "\(signEmpid!)","sign_emp_name" : "\(signEmpName!)" , "trackid" : "","ta_jobid" : "\(x.jobID!)" , "userid" : "\(id!)" , "ta_form_id" : "\(x.taFormID!)" , "clientid" : "\(x.taFormID!)" , "driverid" : "\(x.taDriverID!)" , "jobstatus" : "DELIVERED" , "description" : "" , "trackdatetime" : "\(defaultTimeZoneStr)" , "currentlocation" : "\(address)" ,"destinationlocation" : "0,0" , "pickuplocation" : "0,0" , "latitude" : "\(lat)" , "longitude" : "\(long)" , "status" : "active" , "createby" : "\(defaults.string(forKey: userdefaultsKey.name)!)", "modifyby" : "\(x.taDriverName!)" , "modifydate" : "\(defaultTimeZoneStr)" ,"deviceid" : "" , "bearing" : "\(bearing)" , "bearingaccuracydegrees" : "\(bearingDegree)" , "speed" : "\(speed)" , "speedaccuracymeterspersecond" : "\(speedAccur)" , "accuracy" : "\(accur)" , "jobnumber" : "\(x.jobNumber!)" , "battery_type" : "\(batteryType)" , "battery_percentage" : "\(Int(level*100))" , "network_type" : "\(getConnectionType())" , "network_operator" : "\(carrierName ?? "")" , "geofence" : "EXIT Warehouse" , "support_driver_flag" : "" , "epod_job_id" : "\(x.epodJobID!)" , "main_taform_id" : "\(x.mainTaFormId!)"]
            let imageData = activeAttachImage!.jpegData(compressionQuality: compressionAmount)
            let signData = signatureImage!.jpegData(compressionQuality: 0.1)
            
            print(json)
           
            uploadImageDelivered(image: imageData!, to: URL(string: "\(ConstantsUsedInProject.baseUrl)api/addtrackwithflag")!, params: json, imageSign: signData!)
        }
        else
        {
        if status == "DEPARTURE"
        {

            let json: [String: Any] = ["sign_emp_id" : "","sign_emp_name" : "" , "trackid" : "","ta_jobid" : "\(x.jobID!)" , "userid" : "\(id!)" , "ta_form_id" : "\(x.taFormID!)" , "clientid" : "\(x.taFormID!)" , "driverid" : "\(x.taDriverID!)" , "jobstatus" : "DEPARTURE" , "description" : "" , "trackdatetime" : "\(defaultTimeZoneStr)" , "currentlocation" : "\(address)" ,"destinationlocation" : "0,0" , "pickuplocation" : "0,0" , "latitude" : "\(lat)" , "longitude" : "\(long)" , "status" : "active" , "createby" : "\(defaults.string(forKey: userdefaultsKey.name)!)", "modifyby" : "\(x.taDriverName!)" , "modifydate" : "\(defaultTimeZoneStr)" ,"deviceid" : "" , "bearing" : "\(bearing)" , "bearingaccuracydegrees" : "\(bearingDegree)" , "speed" : "\(speed)" , "speedaccuracymeterspersecond" : "\(speedAccur)" , "accuracy" : "\(accur)" , "jobnumber" : "\(x.jobNumber!)" , "battery_type" : "\(batteryType)" , "battery_percentage" : "\(Int(level*100))" , "network_type" : "\(getConnectionType())" , "network_operator" : "\(carrierName ?? "")" , "geofence" : "EXIT Warehouse" , "support_driver_flag" : "" , "epod_job_id" : "\(x.epodJobID!)" , "main_taform_id" : "\(x.mainTaFormId!)"]
            let imageData = activeAttachImage!.jpegData(compressionQuality: compressionAmount)

            uploadImageDeparture(image: imageData!, to: URL(string: "\(ConstantsUsedInProject.baseUrl)api/addtrackwithflag")!, params: json)
        }
        else{
            delegate?.pauseTimer()
            let json: [String: Any] = ["sign_emp_id" : "","sign_emp_name" : "" , "trackid" : "","ta_jobid" : "\(x.jobID!)" , "userid" : "\(id!)" , "ta_form_id" : "\(x.taFormID!)" , "clientid" : "\(x.taFormID!)" , "driverid" : "\(x.taDriverID!)" , "jobstatus" : "PICKED UP" , "description" : "" , "trackdatetime" : "\(defaultTimeZoneStr)" , "currentlocation" : "\(address)" ,"destinationlocation" : "0,0" , "pickuplocation" : "0,0" , "latitude" : "\(lat)" , "longitude" : "\(long)" , "status" : "active" , "createby" : "\(defaults.string(forKey: userdefaultsKey.name)!)", "modifyby" : "\(x.taDriverName!)" , "modifydate" : "\(defaultTimeZoneStr)" ,"deviceid" : "" , "bearing" : "\(bearing)" , "bearingaccuracydegrees" : "\(bearingDegree)" , "speed" : "\(speed)" , "speedaccuracymeterspersecond" : "\(speedAccur)" , "accuracy" : "\(accur)" , "jobnumber" : "\(x.jobNumber!)" , "battery_type" : "\(batteryType)" , "battery_percentage" : "\(Int(level*100))" , "network_type" : "\(getConnectionType())" , "network_operator" : "\(carrierName ?? "")" , "geofence" : "EXIT Warehouse" , "support_driver_flag" : "", "epod_job_id" : "\(x.epodJobID!)" , "main_taform_id" : "\(x.mainTaFormId!)"]
            let imageData = activeAttachImage!.jpegData(compressionQuality: compressionAmount)
            uploadImage(image: imageData!, to: URL(string: "\(ConstantsUsedInProject.baseUrl)api/addtrackwithflag")!, params: json)
        }
        }

    }
    func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
    func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }

    func getBearingBetweenTwoPoints1(point1 : CLLocation, point2 : CLLocation) -> (Double , Double) {

        let lat1 = degreesToRadians(degrees: point1.coordinate.latitude)
        let lon1 = degreesToRadians(degrees: point1.coordinate.longitude)

        let lat2 = degreesToRadians(degrees: point2.coordinate.latitude)
        let lon2 = degreesToRadians(degrees: point2.coordinate.longitude)

        let dLon = lon2 - lon1

        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)
        //print(radiansBearing)
        return (radiansToDegrees(radians: radiansBearing) , radiansBearing)
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            currentLocation = location
            getAdressName(coords: location)
        }
        
    }

    func uploadImage(image: Data, to url: URL, params: [String: Any]) {
        
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                /*  if let data = value.data(using: .utf8) {
                      multipart.append(data, withName: key)
                      print(String(data: data, encoding: String.Encoding.utf8) as Any)

                  } */
                  if let data = value.removingPercentEncoding?.data(using: .utf8) {
                     // do your stuff here
                      multipart.append(data, withName: key)
                      //print(String(data: data, encoding: String.Encoding.utf8) as Any)
                  }
                              }
            print("pickup")
            let date = NSDate()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //    formatter.timeZone = TimeZone(abbreviation: "UTC")
            let defaultTimeZoneStr = formatter.string(from: date as Date)
            multipart.append(image, withName: "attachmentimage", fileName: "attachnaveen\(defaultTimeZoneStr)", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {

        
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")

                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(JobDepartureDeliveryJSON.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                        
                        DispatchQueue.main.async { [self] in
                            removeSpinnerView()
                            if code_str == 200 {
                                
                                let defaults = UserDefaults.standard
                                defaults.setValue("\(jobData!.jobNumber!)", forKey: "\(userdefaultsKey.jobNumber)")
                                defaults.setValue("true", forKey: "\(userdefaultsKey.jobSelected)")
                                defaults.setValue("\(jobData!.taFormID!)", forKey: "\(userdefaultsKey.cliendid)")
                                defaults.setValue("\(jobData!.taDriverID!)", forKey: "\(userdefaultsKey.driverID)")
                                defaults.setValue("PICKED UP", forKey: "\(userdefaultsKey.jobStatus)")
                                defaults.setValue("\(jobData!.jobID!)", forKey: "\(userdefaultsKey.jobid)")
                                defaults.setValue("\(jobData?.taFormID ?? "")", forKey: "\(userdefaultsKey.taformid)")
                                defaults.setValue("\(jobData?.epodJobID ?? "")", forKey: "\(userdefaultsKey.epodJobid)")
                                defaults.setValue("\(jobData!.taFormID!)", forKey: "\(userdefaultsKey.userID)")
                                defaults.setValue("\(jobData!.createby!)", forKey: "\(userdefaultsKey.createBy)")
                                defaults.setValue("\(jobData!.mainTaFormId!)", forKey: "\(userdefaultsKey.mainTaFormID)")

                                print("success")
                                let alert = UIAlertController(title: "Job", message: "Start To Departure Now", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "Departure", style: UIAlertAction.Style.default, handler: { (_) in
                                    self.prepareForUpload(status: "DEPARTURE")
                                }))
                                delegate?.refresh()
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                          

                                
                            }
                            else
                            {
                                let alert = UIAlertController(title: "Job", message: "\(loginBaseResponse?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }
    
    func uploadImageDeparture(image: Data, to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                /*  if let data = value.data(using: .utf8) {
                      multipart.append(data, withName: key)
                      print(String(data: data, encoding: String.Encoding.utf8) as Any)

                  } */
                  if let data = value.removingPercentEncoding?.data(using: .utf8) {
                     // do your stuff here
                      multipart.append(data, withName: key)
                      //print(String(data: data, encoding: String.Encoding.utf8) as Any)
                  }
                              }
           // multipart.append(image, withName: "attachmentimage", fileName: "attach", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {

        
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(JobDepartureDeliveryJSON.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                        
                        DispatchQueue.main.async { [self] in
                            removeSpinnerView()
                            if code_str == 200 {
                                print("success")
                                let defaults = UserDefaults.standard
                                defaults.setValue("\(jobData!.taFormID!)", forKey: "\(userdefaultsKey.cliendid)")
                                defaults.setValue("\(jobData!.taDriverID!)", forKey: "\(userdefaultsKey.driverID)")
                                defaults.setValue("IN TRANSIT", forKey: "\(userdefaultsKey.jobStatus)")
                                defaults.setValue("\(jobData!.jobNumber!)", forKey: "\(userdefaultsKey.jobNumber)")
                                defaults.setValue("\(jobData!.jobID!)", forKey: "\(userdefaultsKey.jobid)")
                                defaults.setValue("\(jobData?.taFormID ?? "")", forKey: "\(userdefaultsKey.taformid)")
                                defaults.setValue("\(jobData?.epodJobID ?? "")", forKey: "\(userdefaultsKey.epodJobid)")

                                defaults.setValue("\(jobData!.taFormID!)", forKey: "\(userdefaultsKey.userID)")
                                defaults.setValue("true", forKey: "\(userdefaultsKey.jobSelected)")

                                defaults.setValue("\(jobData!.createby!)", forKey: "\(userdefaultsKey.createBy)")
                                departureFlag = true
                                activeAttachImage = nil
                                activeImageview.image = nil
                                
                                toattachIamge.image = nil
                                self.isInTransit()
                                delegate?.refresh()
                                self.activeStatus.text = "DEPARTURE"
                                self.jobData?.jobStatus = "IN TRANSIT"
                                delegate?.resumeTimer()
                                self.navigationController?.popViewController(animated: true)
                                delegate?.refresh()
                                
                            }
                            else
                            {
                                let alert = UIAlertController(title: "Job", message: "\(loginBaseResponse?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }
    func uploadImageDelivered(image: Data, to url: URL, params: [String: Any] , imageSign : Data) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                /*  if let data = value.data(using: .utf8) {
                      multipart.append(data, withName: key)
                      print(String(data: data, encoding: String.Encoding.utf8) as Any)

                  } */
                  if let data = value.removingPercentEncoding?.data(using: .utf8) {
                     // do your stuff here
                      multipart.append(data, withName: key)
                      //print(String(data: data, encoding: String.Encoding.utf8) as Any)
                  }
                              }
            let date = NSDate()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //    formatter.timeZone = TimeZone(abbreviation: "UTC")
            let defaultTimeZoneStr = formatter.string(from: date as Date)
            multipart.append(image, withName: "attachmentimage", fileName: "attachnaveen\(defaultTimeZoneStr)", mimeType: "image/png")
            
            multipart.append(imageSign, withName: "signimage", fileName: "sign\(defaultTimeZoneStr)", mimeType: "image/png")

        }
        
        DispatchQueue.main.async
        {

        
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(JobDepartureDeliveryJSON.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                        
                        DispatchQueue.main.async { [self] in
                            removeSpinnerView()
                            if code_str == 200 {
                                print("success")
                                let defaults = UserDefaults.standard
                                defaults.setValue("\(jobData!.taFormID!)", forKey: "\(userdefaultsKey.cliendid)")
                                defaults.setValue("\(jobData!.taDriverID!)", forKey: "\(userdefaultsKey.driverID)")
                                defaults.setValue("IN TRANSIT", forKey: "\(userdefaultsKey.jobStatus)")
                                defaults.setValue("\(jobData!.jobNumber!)", forKey: "\(userdefaultsKey.jobNumber)")
                                defaults.setValue("\(jobData!.jobID!)", forKey: "\(userdefaultsKey.jobid)")
                                defaults.setValue("\(jobData?.taFormID ?? "")", forKey: "\(userdefaultsKey.taformid)")
                                defaults.setValue("\(jobData?.epodJobID ?? "")", forKey: "\(userdefaultsKey.epodJobid)")
                                defaults.setValue("\(jobData!.mainTaFormId!)", forKey: "\(userdefaultsKey.mainTaFormID)")

                                defaults.setValue("\(jobData!.taFormID!)", forKey: "\(userdefaultsKey.userID)")
                                defaults.setValue("true", forKey: "\(userdefaultsKey.jobSelected)")

                                defaults.setValue("\(jobData!.createby!)", forKey: "\(userdefaultsKey.createBy)")
                                defaults.setValue("false", forKey: "\(userdefaultsKey.jobSelected)")
                                self.delegate?.jobDelivered()
                             //   jobDoneFlag = true
                               // fromIsActive()

                                let alert = UIAlertController(title: "Job", message: "Done", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                                delegate?.refresh()
                                
                              //  self.navigationController?.popViewController(animated: true)

                                
                            }
                            else
                            {
                                let alert = UIAlertController(title: "Job", message: " code \(code_str!) \(loginBaseResponse?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }

}







extension JobDetails : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    func openCamera()
    {
        isCamera = true
        let imagePicker = UIImagePickerController()
        
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let pickedImage = info[.editedImage] as? UIImage else {
            // imageViewPic.contentMode = .scaleToFill
            print("No image found")
            return
        }
        //journalImageView.image = pickedImage
        saveImage(image: pickedImage)
    }
    func openGallery()
    {
        isCamera = false
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true

        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    func saveImage(image : UIImage)
    {
       // let id = UserDefaults.standard.string(forKey: userDefaultsKey.userMemberID)!
        //let json: [String: Any] = ["user_memberid" : "\(id)"]
        //profileImage = image
        activeAttachImage = image
        activeImageview.image = image
        activeImageview.contentMode = .scaleAspectFill
        toattachIamge.image = image
        toattachIamge.contentMode = .scaleAspectFill
        fromDepartureButton(departureOrDeliveryBUtton as Any)
        //profileImageView.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 0)
        //uploadImage(image: imageData!, to: URL(string: "http://thefollo.com/housing/housing_android_api/Imagehelper/users_profileupload")!, params: json)
        
    }
}

extension URLEncoding {
    public func queryParameters(_ parameters: [String: Any]) -> [(String, String)] {
        var components: [(String, String)] = []
        
        for key in parameters.keys.sorted(by: <) {
            let value = parameters[key]!
            components += queryComponents(fromKey: key, value: value)
        }
        return components
    }
}

protocol jobFinishedDelegate {
    func jobDelivered()
    func refresh()
    func pauseTimer()
    func resumeTimer()
}
