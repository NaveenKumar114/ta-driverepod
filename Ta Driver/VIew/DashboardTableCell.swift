//
//  DashboardTableCell.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-05-17.
//

import UIKit

class DashboardTableCell: UITableViewCell {
    @IBOutlet weak var viewWithImage: UIView!

    @IBOutlet weak var companyName: UILabel!
    
    @IBOutlet weak var truckNumber: UILabel!
    @IBOutlet weak var taNumber: UILabel!
    @IBOutlet weak var truckType: UILabel!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var locImage: UIImageView!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var buttonLabel: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var to: UILabel!
    @IBOutlet weak var jobNuber: UILabel!
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var commission: UILabel!
    @IBOutlet weak var fromAddress: UILabel!
    @IBOutlet weak var toAddress: UILabel!
    @IBOutlet weak var payToDriver: UILabel!
    @IBOutlet weak var payToDriverText: UILabel!
    @IBOutlet weak var commissionText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewWithImage.layer.borderWidth = 1
        viewWithImage.layer.borderColor = UIColor.white.cgColor
        viewWithImage.layer.cornerRadius = viewWithImage.frame.height / 2
        buttonView.layer.cornerRadius = 5
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
