//
//  ExpenseListCell.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-27.
//

import UIKit

class ExpenseListCell: UITableViewCell {

    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var viewForBackground: UIView!
    @IBOutlet weak var mode: UILabel!
    @IBOutlet weak var circleVIew: UIView!
    @IBOutlet weak var expenseIcon: UIImageView!
    @IBOutlet weak var expenseImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var remarks: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
