//
//  RoutesCollectionCell.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-05-21.
//

import UIKit

class RoutesCollectionCell: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var est: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var minutes: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

}
