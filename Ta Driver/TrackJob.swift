//
//  TrackJob.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-26.
//

import UIKit
import MapKit
class TrackJob: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var to: UILabel!
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var commission: UILabel!
    @IBOutlet weak var viewWithImage: UIView!
    @IBOutlet weak var fromAddress: UILabel!
    @IBOutlet weak var toAddress: UILabel!
    @IBOutlet weak var payToDriver: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var commissionTExt: UILabel!
    @IBOutlet weak var payToDriverTExt: UILabel!
    
    
    @IBOutlet weak var date: UILabel!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trackData?.trackList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tackTableView.dequeueReusableCell(withIdentifier: "JobListCell") as! JobListCell
        let i = ((trackData?.trackList!.count)! - 1) - indexPath.row
        if let x = trackData?.trackList?[i]
        {
           
            print(x.jobstatus as Any)
            print(x.currentlocation as Any)
           // cell.address.text = x.pickuplocation ?? ""
            let d = convertDateFormater(x.trackdatetime!)
            cell.date.text = d.0
            cell.time.text = d.1

            cell.status.text = x.jobstatus ?? ""
            cell.circleVIew.layer.cornerRadius = cell.circleVIew.frame.width / 2
           
            switch x.jobstatus!  {
            case "ASSIGNED":
               // cell.iconImage.image = UIImage(named: "")?.withTintColor(.white)
            print("ee")
            case "COLLECTING":
                cell.iconImage.image = UIImage(named: "black_collecting")?.withTintColor(.white)
                cell.circleVIew.applyGradient(color: .blue)

            case "PICKED UP":
                cell.iconImage.image = UIImage(named: "black_pickedup2")?.withTintColor(.white)
                cell.circleVIew.applyGradient(color: .red)

            case "DEPARTURE":
                cell.iconImage.image = UIImage(named: "black_departure")?.withTintColor(.white)
                cell.circleVIew.applyGradient(color: .systemBlue)

            case "IN TRANSIT":
                cell.iconImage.image = UIImage(named: "black_transit")?.withTintColor(.white)
                cell.circleVIew.applyGradient(color: .black)

            case "DELIVERED":
                cell.iconImage.image = UIImage(named: "black_delivered")?.withTintColor(.white)
                cell.circleVIew.applyGradient(color: .green)
            case "PICKUP ARRANGED":
                cell.iconImage.image = UIImage(named: "black_pickup_arranged")?.withTintColor(.white)
                cell.circleVIew.applyGradient(color: .yellow)
            default:
                cell.iconImage.image = UIImage(named: "black_pickedup")?.withTintColor(.white)
                cell.circleVIew.applyGradient(color: .yellow)

            }
            if x.currentlocation != nil || x.currentlocation != ""
            {
              
                cell.address.text = x.currentlocation ?? ""
            }
            else
            {
            if x.latitude != nil && x.longitude != nil && x.latitude != "" && x.longitude != ""
            {
                let lat : CLLocationDegrees = Double(x.latitude!)!
                let long : CLLocationDegrees = Double(x.longitude!)!

            let cityCoords = CLLocation(latitude: lat, longitude: long)
                getAdressName(coords: cityCoords, label: cell.address)
            }
            }
                cell.driverName.text = x.drivername ?? ""
            
        }
            
        
        return cell
    }
    func getAdressName(coords: CLLocation , label : UILabel) {

        CLGeocoder().reverseGeocodeLocation(coords) { (placemark, error) in
                if error != nil {
                    print("Hay un error")
                } else {

                    let place = placemark! as [CLPlacemark]
                    if place.count > 0 {
                        let place = placemark![0]
                        var adressString : String = ""
                        if place.thoroughfare != nil {
                            adressString = adressString + place.thoroughfare! + ", "
                        }
                        if place.subThoroughfare != nil {
                            adressString = adressString + place.subThoroughfare! + "\n"
                        }
                        if place.locality != nil {
                            adressString = adressString + place.locality! + " - "
                        }
                        if place.postalCode != nil {
                            adressString = adressString + place.postalCode! + "\n"
                        }
                        if place.subAdministrativeArea != nil {
                            adressString = adressString + place.subAdministrativeArea! + " - "
                        }
                        if place.country != nil {
                            adressString = adressString + place.country!
                        }
                        label.text = adressString
                        print(adressString)
                    }
                }
            }
      }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func convertDateFormater(_ date: String) -> (String , String)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "h:mm a"
            let t = dateFormatter.string(from: date!)

            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  (dateFormatter.string(from: date!) , t)

        }
    func convertDateFormaterDate(_ date: String) -> (String)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
        //dateFormatter.dateFormat = "h:mm a"
            //let t = dateFormatter.string(from: date!)

            dateFormatter.dateFormat = "dd-MMM-yyyy"
            return  (dateFormatter.string(from: date!))

        }

    var jobData : Datum?
    var trackData : TrackListJSON?
    @IBOutlet weak var tackTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
makeGetCall()
        viewWithImage.layer.borderWidth = 1
        viewWithImage.layer.borderColor = UIColor.white.cgColor
        viewWithImage.layer.cornerRadius = viewWithImage.frame.height / 2
        let x = jobData
        tackTableView.delegate = self
        tackTableView.dataSource = self
        tackTableView.register(UINib(nibName: "JobListCell", bundle: nil) , forCellReuseIdentifier: "JobListCell")
        let y = Double(x?.totalPaytodriver ?? "0.0")
        let s = String(format: "%.2f", y ?? 0.0)
       payToDriver.text = "MYR \(s)"
        commission.text = "MYR \(x?.jobCommision ?? "")"
        from.text = x?.shipperName?.uppercased()
        to.text = x?.consigneeName?.uppercased()
        fromAddress.text = x?.shipperLocationaddress
        toAddress.text = x?.consigneeLocationaddress
        status.text = jobData?.jobStatus ?? ""
        date.text = convertDateFormaterDate((x?.jobDate!)!)
        //client.text = jobData?.jobClientName?.uppercased() ?? ""
        self.title = jobData?.jobNumber ?? ""
        // Do any additional setup after loading the view.
        self.tackTableView.tableFooterView = UIView()
        //tackTableView.separatorStyle = .none
        let vendor = UserDefaults.standard.string(forKey: userdefaultsKey.vendorOptionDriver)?.uppercased()
        if vendor == "OTHER"
        {
            payToDriverTExt.isHidden = true
            payToDriver.isHidden = true
            commissionTExt.text = "Rental"
            let y = Double(x?.jobCommision ?? "0.0")
            let s = String(format: "%.2f", y ?? 0.0)
            commission.text = "MYR \(s)"
            
        }
    }
    
    func makeGetCall() {
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(ConstantsUsedInProject.baseUrl)track/index_get/\(jobData!.taFormID!)/\(jobData!.taDriverID!)/2021-04-26")! as URL)
        print("\(ConstantsUsedInProject.baseUrl)track/index_get/\(jobData!.taFormID!)/\(jobData!.taDriverID!)/2021-04-26")
        request.httpMethod = "GET"
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            do {
                let responseJ = try? decoder.decode(TrackListJSON.self, from: data!)
                let code_str = responseJ?.code
                print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                DispatchQueue.main.async {
                    if code_str == 200 {
                        print("success")
                        print(responseJ as Any)
                        self.trackData = responseJ
                        self.tackTableView.reloadData()
                        self.status.text = self.trackData?.trackList?.first?.jobstatus ?? ""

                    }else if code_str == 201  {
                        let alert = UIAlertController(title: "Track", message: "\(responseJ?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        task.resume()
    }
  

}
