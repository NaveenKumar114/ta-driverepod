//
//  TrackInMap.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-05-19.
//

import UIKit
import MapKit

class TrackInMap: UIViewController, MKMapViewDelegate {
    var jobData : Datum?

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusTitle: UILabel!
    @IBOutlet weak var durationTit: UILabel!
    @IBOutlet weak var distnceTite: UILabel!
    @IBOutlet weak var multiRoutesCollection: UICollectionView!
    @IBOutlet weak var estTimeLabel: UILabel!
    @IBOutlet weak var trackMap: MKMapView!
    @IBOutlet weak var estDistanceLabel: UILabel!
    var routesData : [MKRoute]?
    var overlaysArray : [MKOverlay]?
    var selectedRoute = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        trackMap.delegate = self
        var lat = Double(jobData?.fromLatitude ?? "0") ?? 0.0
        var long = Double(jobData?.fromLongitude ?? "0") ?? 0.0
        let f = CLLocation(latitude: lat, longitude: long)
       // print("Shipper -  lat \(lat) , long \(long)")
        let fromLOC = CLLocationCoordinate2D(latitude: lat, longitude: long)
         lat = Double(jobData?.toLatitude ?? "0") ?? 0.0
         long = Double(jobData?.toLongitude ?? "0") ?? 0.0
     //   print("Consignee -  lat \(lat) , long \(long)")
     //   print(jobData)

        let toLOC = CLLocationCoordinate2D(latitude: lat, longitude: long)
       // trackMap.showRouteOnMap(pickupCoordinate: fromLOC, destinationCoordinate: toLOC)
        let t = CLLocation(latitude: lat, longitude: long)
        let distanceInMeters = f.distance(from: t)
        let d = Double(distanceInMeters) / 1000
        estDistanceLabel.text = "\(Int(d)) KM"
        
        estTimeLabel.text = "0"
        multiRoutesCollection.delegate = self
        multiRoutesCollection.dataSource = self
        multiRoutesCollection.register(UINib(nibName: "RoutesCollectionCell", bundle: nil), forCellWithReuseIdentifier: "RoutesCollectionCell")
        getRoutes(pickupCoordinate: fromLOC, destinationCoordinate: toLOC)
        var status = ""
        if jobData?.jobStatus?.uppercased() == "COLLECTING"
        {
          status = "PICKUP"
        }
        else
        {
            status = "DELIVERY"
        }
        statusLabel.text = "\(jobData?.jobStatus?.uppercased() ?? "")"
        statusTitle.text = "ON THE WAY FOR \(status) ITEM"
        durationTit.text = status
        distnceTite.text = status
        

    }


    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        switch overlay {
        case let polyline as MKPolyline:
            let renderer = MKPolylineRenderer(polyline: polyline)
            renderer.strokeColor = ConstantsUsedInProject.appThemeColor
            renderer.lineWidth = 5
            return renderer

        // you can add more `case`s for other overlay types as needed

        default:
            fatalError("Unexpected MKOverlay type")
        }
    }
    
    @IBAction func goNowPressed(_ sender: Any) {
        var lat = Double(jobData?.fromLatitude ?? "0") ?? 0.0
        var long = Double(jobData?.fromLongitude ?? "0") ?? 0.0
        
       

        let source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long)))
        source.name = "Source"
                
        lat = Double(jobData?.toLatitude ?? "0") ?? 0.0
        long = Double(jobData?.toLongitude ?? "0") ?? 0.0
        let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long)))
        destination.name = "Destination"
                
        MKMapItem.openMaps(
          with: [source, destination],
          launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        )
    }
    @IBAction func endNowPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setOverlays()
    {
        if let x = routesData
        {
            let overlays = trackMap.overlays
            trackMap.removeOverlays(overlays)
            for i in 0 ..< x.count
            {
                let route = x[i]
                let o : MKOverlay = route.polyline
                overlaysArray?.append(o)
                if i == selectedRoute
                {
                trackMap.addOverlay((o), level: MKOverlayLevel.aboveRoads)
                }
            }
            let rect = x[0].polyline.boundingMapRect
            trackMap.setRegion(MKCoordinateRegion(rect), animated: true)
        }
    }
    func getRoutes(pickupCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D) {
        let sourcePlacemark = MKPlacemark(coordinate: pickupCoordinate, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate, addressDictionary: nil)
        
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        
        let sourceAnnotation = MKPointAnnotation()
        
        if let location = sourcePlacemark.location {
            sourceAnnotation.coordinate = location.coordinate
        }
        
        let destinationAnnotation = MKPointAnnotation()
        
        if let location = destinationPlacemark.location {
            destinationAnnotation.coordinate = location.coordinate
        }
        
        trackMap.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
        
        let directionRequest = MKDirections.Request()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile
        directionRequest.requestsAlternateRoutes = true
        
        // Calculate the direction
        let directions = MKDirections(request: directionRequest)
        
        directions.calculate {
            (response, error) -> Void in
            
            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                
                return
            }
            self.routesData = response.routes
            self.multiRoutesCollection.reloadData()
            self.setOverlays()
    }
}
}


extension TrackInMap : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return routesData?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
      
        
            let cell = multiRoutesCollection.dequeueReusableCell(withReuseIdentifier: "RoutesCollectionCell", for: indexPath) as! RoutesCollectionCell
        if let x = routesData?[indexPath.row]
       {
            let d = Double(x.distance) / 1000
            cell.distance.text = "\(Int(d)) km"
            let ti = NSInteger(x.expectedTravelTime)

         //     let seconds = ti % 60
              let minutes = (ti / 60) % 60
              let hours = (ti / 3600)
            cell.est.text = "\(hours) hours"
            cell.minutes.text = "\(minutes) mins"
            if indexPath.row == 0
            {
                cell.mainView.backgroundColor = .green
                estDistanceLabel.text = "\(Int(d)) km"
                
                estTimeLabel.text = "\(hours) hours \(minutes) mins"
            }
            else
            {
                cell.mainView.backgroundColor = #colorLiteral(red: 0.8827351928, green: 0.8866485953, blue: 0.8936274648, alpha: 1)
            }
            

       }
        cell.mainView.layer.cornerRadius = 5

            return cell
        

        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      
          
           // let h = dashboardCollection.frame.height
            return CGSize(width: view.frame.width / 3.34, height: 90)
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for x in 0 ..< routesData!.count
        {
            let i = IndexPath(row: x, section: 0)
            let cell = multiRoutesCollection.cellForItem(at: i) as! RoutesCollectionCell
            if indexPath.row == x
            {
                if let x = routesData?[indexPath.row]
               {
                    let d = Double(x.distance) / 1000
                    let ti = NSInteger(x.expectedTravelTime)

                      //let seconds = ti % 60
                      let minutes = (ti / 60) % 60
                      let hours = (ti / 3600)
                        estDistanceLabel.text = "\(Int(d)) km"
                        estTimeLabel.text = "\(hours) hours \(minutes) mins"

               }
                cell.mainView.backgroundColor = .green
            }
            else
            {
                cell.mainView.backgroundColor = #colorLiteral(red: 0.8827351928, green: 0.8866485953, blue: 0.8936274648, alpha: 1)
            }
          
            
        }
        selectedRoute = indexPath.row
        setOverlays()
    }
}

extension MKMapView {

  func showRouteOnMap(pickupCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D) {
    let sourcePlacemark = MKPlacemark(coordinate: pickupCoordinate, addressDictionary: nil)
    let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate, addressDictionary: nil)
    
    let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
    let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
    
    let sourceAnnotation = MKPointAnnotation()
    
    if let location = sourcePlacemark.location {
        sourceAnnotation.coordinate = location.coordinate
    }
    
    let destinationAnnotation = MKPointAnnotation()
    
    if let location = destinationPlacemark.location {
        destinationAnnotation.coordinate = location.coordinate
    }
    
    self.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
    
    let directionRequest = MKDirections.Request()
    directionRequest.source = sourceMapItem
    directionRequest.destination = destinationMapItem
    directionRequest.transportType = .automobile
    directionRequest.requestsAlternateRoutes = true
    
    // Calculate the direction
    let directions = MKDirections(request: directionRequest)
    
    directions.calculate {
        (response, error) -> Void in
        
        guard let response = response else {
            if let error = error {
                print("Error: \(error)")
            }
            
            return
        }
        
        var route = response.routes[0]
        self.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)
        var rect = route.polyline.boundingMapRect
        self.setRegion(MKCoordinateRegion(rect), animated: true)
        
         route = response.routes[1]
        self.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)
         rect = route.polyline.boundingMapRect
        self.setRegion(MKCoordinateRegion(rect), animated: true)
        
    }
}}
