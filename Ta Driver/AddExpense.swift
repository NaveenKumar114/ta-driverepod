//
//  AddExpense.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-28.
//

import UIKit
import Alamofire
import CoreLocation

class AddExpense: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate {
    var jobData : Datum?
    var textfieldPickers = [UITextField]()
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    override func viewWillDisappear(_ animated: Bool) {
      //  super.viewWillDisappear(true)
      
       // self.navigationController?.navigationBar.isTranslucent = false
        locationManager?.stopUpdatingLocation()
   
    }
    func createSpinnerView() {
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func removeSpinnerView()
    {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    let child = SpinnerViewController()

    var editData : Expense?
    var delegate : addExpenseProtocol?
    
    @IBOutlet weak var typeTextField: UITextField!
    @IBOutlet weak var expenseImageView: UIImageView!
    var expensesType : ExpenseTypeJSON?
   
    @IBOutlet weak var typeSegment: UISegmentedControl!
    @IBOutlet weak var amountTextField: UITextField!
    var selectedExpense : expenseType = .cash
    var expenseImage : UIImage?
    var locationManager: CLLocationManager?
    var currentLocation : CLLocation?
    var address = ""

    let toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 400, width: UIScreen.main.bounds.size.width, height: 50))

    var modeEntries = ["CASH" , "CARD" , "ONLINE"]
    var typeEntries = ["FUEL" , "TOUCH N GO" , "FORKLIFT" , "LEVY" , "AUTO PASS" , "TRANSPORT" , "HOTEL" , "FOOD"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeGetCallExpenseType()
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        if  locationManager?.location?.coordinate.latitude != nil
        {
            _ = String((locationManager?.location?.coordinate.latitude)!)
            _ = String((locationManager?.location?.coordinate.longitude)!)
            _ = CLLocation(latitude: (locationManager?.location?.coordinate.latitude)!, longitude: (locationManager?.location?.coordinate.longitude)!)
           // getAdressName(coords: (cl))

        }
    
        locationManager?.requestLocation()
        locationManager?.startUpdatingLocation()
        amountTextField.delegate = self
        amountTextField.tag = 100
        textfieldPickers = [typeTextField]
        typeTextField.text = ""
        for textField in 0 ... textfieldPickers.count - 1
        {
            textfieldPickers[textField].delegate = self
            createPickerView(textField: textfieldPickers[textField], tag: textField)
            //textfieldPickers[textField].font = UIFont(name: "Arial-Rounded", size: 15.0)

        }
        dismissPickerView()
        dismissPickerViewForExpense()
        
        let gestureident = UITapGestureRecognizer(target: self, action: #selector(toImage))
        expenseImageView.isUserInteractionEnabled = true
        expenseImageView.addGestureRecognizer(gestureident)

        typeSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        typeSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: ConstantsUsedInProject.appThemeColor], for: .normal)
        // Do any additional setup after loading the view.
       if editData != nil
        {
            typeTextField.text = editData?.expenseType ?? ""
            
            if editData?.expenseCash == "0.00"
            {
                typeSegment.selectedSegmentIndex = 1
                amountTextField.text = editData?.expenseCard
                selectedExpense = .card
            }
        else
            {
                typeSegment.selectedSegmentIndex = 0
                amountTextField.text = editData?.expenseCash
                selectedExpense = .cash
            }
            if editData?.expenseImage != nil
            {
                if editData?.expenseImage != ""
                {
                let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)expense/\(editData!.expenseImage!)")
                print(urlStr)
                let url = URL(string: urlStr)
                
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async { [self] in
                        expenseImageView.image = UIImage(data: data!)
                       // logoImageVIew.image = UIImage(data: data!)
                      // s logoImageVIew.contentMode = .scaleAspectFit
                        
                    }
                      
                    }
                }
            }

        }
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    locationManager?.requestAlwaysAuthorization()

            case .authorizedAlways, .authorizedWhenInUse:
                    print("Access")
                @unknown default:
                break
            }
            } else {
                let alert = UIAlertController(title: "Expense", message: "Please turn on location", preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)        }
    }
    func dismissPickerViewForExpense() {
       let toolBar = UIToolbar()
       toolBar.sizeToFit()
       let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
       toolBar.setItems([button], animated: true)
       toolBar.isUserInteractionEnabled = true
    amountTextField.inputAccessoryView = toolBar
      
        
    }
    @objc func action() {
          view.endEditing(true)
    }
    @objc func toImage()
    {
        let chooseImageActionMenu = UIAlertController(title: "Choose an option", message: nil, preferredStyle: .actionSheet)
        let galleryButton = UIAlertAction(title: "Gallery", style: .default) { (_) in
            self.openGallery()
        }
        let cameraButton = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.openCamera()
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        chooseImageActionMenu.addAction(galleryButton)
        chooseImageActionMenu.addAction(cameraButton)
        chooseImageActionMenu.addAction(cancelButton)
        self.present(chooseImageActionMenu, animated: true, completion: nil)
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            currentLocation = location
           // getAdressName(coords: location)
        }
    }

    @IBAction func cashOrCCardSegmentChanged(_ sender: Any) {
        let x = sender as! UISegmentedControl
        switch x.selectedSegmentIndex {
        case 0:
            selectedExpense = .cash
        case 1:
            selectedExpense = .card
        default:
            print("errorseg")
        }
    }
    func getAdressName(coords: CLLocation) {

        CLGeocoder().reverseGeocodeLocation(coords) { [self] (placemark, error) in
                if error != nil {
                    print("\(String(describing: error))")
                } else {

                    let place = placemark! as [CLPlacemark]
                    if place.count > 0 {
                        let place = placemark![0]
                        var adressString : String = ""
                        if place.thoroughfare != nil {
                            adressString = adressString + place.thoroughfare! + ", "
                        }
                        if place.subThoroughfare != nil {
                            adressString = adressString + place.subThoroughfare! + ", "
                        }
                        if place.locality != nil {
                            adressString = adressString + place.locality! + " - "
                        }
                        if place.postalCode != nil {
                            adressString = adressString + place.postalCode! + ", "
                        }
                        if place.subAdministrativeArea != nil {
                            adressString = adressString + place.subAdministrativeArea! + " - "
                        }
                        if place.country != nil {
                            adressString = adressString + place.country!
                        }
                        self.address = adressString
                        print(address)
                    }
                }
            }
      }

    
    @IBAction func saveButtonPressed(_ sender: Any) {
        var lat = ""
        var long = ""
        
        if currentLocation != nil{
            lat = String((currentLocation?.coordinate.latitude)!)
            long = String((currentLocation?.coordinate.longitude)!)
         

        }
        else
        {
            if  locationManager?.location?.coordinate.latitude != nil
            {
                lat = String((locationManager?.location?.coordinate.latitude)!)
                long = String((locationManager?.location?.coordinate.longitude)!)
                let cl = CLLocation(latitude: (locationManager?.location?.coordinate.latitude)!, longitude: (locationManager?.location?.coordinate.longitude)!)
                //getAdressName(coords: (cl))

            }
         
        }
        if typeTextField.text == "" || amountTextField.text == ""
        {
            let alert = UIAlertController(title: "Expense", message: "Please Enter all details", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        if lat == "" || long == ""
            {
            let alert = UIAlertController(title: "Expense", message: "Please turn on location", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            if CLLocationManager.locationServicesEnabled() {
                switch CLLocationManager.authorizationStatus() {
                    case .notDetermined, .restricted, .denied:
                        locationManager?.requestAlwaysAuthorization()

                case .authorizedAlways, .authorizedWhenInUse:
                        print("Access")
                    @unknown default:
                    break
                }
                } else {
                    let alert = UIAlertController(title: "Expense", message: "Please turn on location", preferredStyle: UIAlertController.Style.alert)
                    
                    // add an action (button)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    
                    // show the alert
                    //self.present(alert, animated: true, completion: nil)
                    
                }
        }
        else
        {
            var cashAmount = "0.00"
            var cardAmount = "0.00"
            if selectedExpense == .cash
            {
                cashAmount = amountTextField.text!
            }
           else
            {
                cardAmount = amountTextField.text!
            }
            
            let date = NSDate()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //    formatter.timeZone = TimeZone(abbreviation: "UTC")
            let defaultTimeZoneStr = formatter.string(from: date as Date)

            let x = jobData!
            let defaults = UserDefaults.standard
            let name = defaults.string(forKey: "name")
           // let id = defaults.string(forKey: "id")
            if editData != nil
            {
                let cl = CLLocation(latitude: (Double(lat))!, longitude: (Double(long))!)
                CLGeocoder().reverseGeocodeLocation(cl) { [self] (placemark, error) in
                        if error != nil {
                            print("\(String(describing: error))")
                        } else {

                            let place = placemark! as [CLPlacemark]
                            if place.count > 0 {
                                let place = placemark![0]
                                var adressString : String = ""
                                if place.thoroughfare != nil {
                                    adressString = adressString + place.thoroughfare! + ", "
                                }
                                if place.subThoroughfare != nil {
                                    adressString = adressString + place.subThoroughfare! + ", "
                                }
                                if place.locality != nil {
                                    adressString = adressString + place.locality! + " - "
                                }
                                if place.postalCode != nil {
                                    adressString = adressString + place.postalCode! + ", "
                                }
                                if place.subAdministrativeArea != nil {
                                    adressString = adressString + place.subAdministrativeArea! + " - "
                                }
                                if place.country != nil {
                                    adressString = adressString + place.country!
                                }
                                self.address = adressString
                                print(address)
                            }
                        }
                    if address == ""
                    {
                        let x = defaults.string(forKey: userdefaultsKey.currentAddress)
                        address = x ?? ""
                    }
                    // 0.00 for non selected
                     let json: [String: Any] = ["expenseid" : "\(editData!.expenseID!)" , "jobid" : "\(x.jobID!)" , "userid" : "\(x.jobIdPrimary!)" , "jobtruckid" : "\(x.taFormID!)" , "driverid" : "\(x.taDriverID!)" , "drivername" : "\(x.taDriverName!)", "username" : "\(name!)","description" : "",
                                                "expensecashamount" : "\(cashAmount)" , "expensecardamount" : "\(cardAmount)", "typeofexpense" : "\(typeTextField.text!)" , "modeofpayment" : "" , "status" : "PENDING" , "createdby" : "\(x.createby!)" , "modifyby" : "\(name!)" ,
                                                "modifydate" : "\(defaultTimeZoneStr)" , "deviceid" : "" , "charge_type" : "\(x.jobChargeType!)","charge_type_id" : "\(x)" , "truck_number" : "\(x.truckNumber!)", "truck_number_id" : "\(x.truckNumberid!)" , "driver_vendor_id" : "" , "driver_vendor_type" : "" , "expense_latitude" : "\(lat)" , "expense_longitude" : "\(long)" , "expense_address" : "\(address)"]
                    createSpinnerView()
                     if expenseImage != nil
                     {
                     let imageData = expenseImage!.jpegData(compressionQuality: 0.10)

                     uploadImageExpenses(image: imageData!, to: URL(string: "\(ConstantsUsedInProject.baseUrl)job/addexpense")!, params: json)
                     }
                     else
                     {
                         expensesNoImage(to: URL(string: "\(ConstantsUsedInProject.baseUrl)job/addexpense")!, params: json)
                     }
                    }

              

            }
            else
            {
                let cl = CLLocation(latitude: (Double(lat))!, longitude: (Double(long))!)
                CLGeocoder().reverseGeocodeLocation(cl) { [self] (placemark, error) in
                        if error != nil {
                            print("\(String(describing: error))")
                        } else {

                            let place = placemark! as [CLPlacemark]
                            if place.count > 0 {
                                let place = placemark![0]
                                var adressString : String = ""
                                if place.thoroughfare != nil {
                                    adressString = adressString + place.thoroughfare! + ", "
                                }
                                if place.subThoroughfare != nil {
                                    adressString = adressString + place.subThoroughfare! + ", "
                                }
                                if place.locality != nil {
                                    adressString = adressString + place.locality! + " - "
                                }
                                if place.postalCode != nil {
                                    adressString = adressString + place.postalCode! + ", "
                                }
                                if place.subAdministrativeArea != nil {
                                    adressString = adressString + place.subAdministrativeArea! + " - "
                                }
                                if place.country != nil {
                                    adressString = adressString + place.country!
                                }
                                self.address = adressString
                                print(address)
                            }
                        }
                    if address == ""
                    {
                        let x = defaults.string(forKey: userdefaultsKey.currentAddress)
                        address = x ?? ""
                    }
                    let json: [String: Any] = ["jobid" : "\(x.jobID!)" , "userid" : "\(x.jobIdPrimary!)" , "jobtruckid" : "\(x.taFormID!)" , "driverid" : "\(x.taDriverID!)" , "drivername" : "\(x.taDriverName!)", "username" : "\(name!)","description" : "",
                                     "expensecashamount" : "\(cashAmount)" , "expensecardamount" : "\(cardAmount)" , "typeofexpense" : "\(typeTextField.text!)" , "modeofpayment" : "" , "status" : "PENDING" , "createdby" : "\(x.createby!)" , "modifyby" : "\(name!)" ,
                                     "modifydate" : "\(defaultTimeZoneStr)" , "deviceid" : "" , "charge_type" : "\(x.jobChargeType!)","charge_type_id" : "\(x)" , "truck_number" : "\(x.truckNumber!)", "truck_number_id" : "\(x.truckNumberid!)" , "driver_vendor_id" : "" , "driver_vendor_type" : "" , "expense_latitude" : "\(lat)" , "expense_longitude" : "\(long)" , "expense_address" : "\(address)"]
        createSpinnerView()
          if expenseImage != nil
          {
          let imageData = expenseImage!.jpegData(compressionQuality: 0.10)

              
          uploadImageExpenses(image: imageData!, to: URL(string: "\(ConstantsUsedInProject.baseUrl)job/addexpense")!, params: json)
          }
          else
          {
              expensesNoImage(to: URL(string: "\(ConstantsUsedInProject.baseUrl)job/addexpense")!, params: json)
          }
                    }
            }
        }
    }
    func makeGetCallExpenseType() {
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(ConstantsUsedInProject.baseUrl)job/getAllExpensetypes")! as URL)
        request.httpMethod = "GET"
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            do {
                let result = try? decoder.decode(ExpenseTypeJSON.self, from: data!)
                let code_str = result?.code
                DispatchQueue.main.async { [self] in
                    if code_str == 200 {
                        expensesType = result
                        //self.typeTextField.text = expensesType?.advances?.first?.advanceName ?? ""
                    }else if code_str == 201  {
                        let alert = UIAlertController(title: "Expense", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        task.resume()
    }
    func uploadImageExpenses(image: Data, to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                /*  if let data = value.data(using: .utf8) {
                      multipart.append(data, withName: key)
                      print(String(data: data, encoding: String.Encoding.utf8) as Any)

                  } */
                  if let data = value.removingPercentEncoding?.data(using: .utf8) {
                     // do your stuff here
                      multipart.append(data, withName: key)
                      //print(String(data: data, encoding: String.Encoding.utf8) as Any)
                  }
                              }
            let date = NSDate()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //    formatter.timeZone = TimeZone(abbreviation: "UTC")
            let defaultTimeZoneStr = formatter.string(from: date as Date)
            multipart.append(image, withName: "userfile", fileName: "expense\(defaultTimeZoneStr)", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {

        
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(DeleteExpenseJSON.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                        
                        DispatchQueue.main.async { [self] in
                            removeSpinnerView()
                                if code_str == 200
                                {
                                let alert = UIAlertController(title: "Expense", message: "Success", preferredStyle: UIAlertController.Style.alert)
                                delegate?.refresh()
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                                }
                            else
                                {
                                    let alert = UIAlertController(title: "Expense", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                                    delegate?.refresh()
                                    // add an action (button)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                        //do
                                    }))
                                    
                                    // show the alert
                                    self.present(alert, animated: true, completion: nil)
                                    }
                                }
                    
                            
                            
                        }

                        
                        
                    }
                }
        
        
        
    }
    func expensesNoImage(to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                /*  if let data = value.data(using: .utf8) {
                      multipart.append(data, withName: key)
                      print(String(data: data, encoding: String.Encoding.utf8) as Any)

                  } */
                  if let data = value.removingPercentEncoding?.data(using: .utf8) {
                     // do your stuff here
                      multipart.append(data, withName: key)
                      //print(String(data: data, encoding: String.Encoding.utf8) as Any)
                  }
                              }
            //multipart.append(image, withName: "userfile", fileName: "userfile", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {

        
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(DeleteExpenseJSON.self, from: data)
                        
                        
                        //let code_str = loginBaseResponse?.header?.status
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                        let code_str = loginBaseResponse?.code

                        DispatchQueue.main.async { [self] in
                            removeSpinnerView()
                                if code_str == 200
                                {
                                let alert = UIAlertController(title: "Expense", message: "Success", preferredStyle: UIAlertController.Style.alert)
                                delegate?.refresh()
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                                }
                            else
                                {
                                    let alert = UIAlertController(title: "Expense", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                                    delegate?.refresh()
                                    // add an action (button)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                        //do
                                    }))
                                    
                                    // show the alert
                                    self.present(alert, animated: true, completion: nil)
                                    }
                                }
                        
                    }
                }
        }
        
        
    }

    func createPickerView(textField : UITextField , tag : Int) {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        textField.inputView = pickerView
        textField.tag = tag
        textField.tintColor = UIColor.clear
        pickerView.tag = tag
    }
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        for textField in textfieldPickers
        {
            textField.inputAccessoryView = toolBar
        }
        
    }
    
  
    

}

extension AddExpense : UIPickerViewDelegate , UIPickerViewDataSource
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text == ""
        {

        switch textField.tag {
        case 0:
            textField.text = expensesType?.advances?[0].advanceName
     
        default:
            print("err")
        }
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return expensesType?.advances?.count ?? 0
     
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            return expensesType?.advances?[row].advanceName
      
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 0:
            textfieldPickers[0].text = expensesType?.advances?[row].advanceName
     
            
            self.view.endEditing(true)

        default:
            print("err")
            print(pickerView.tag)
        }
    }
}

extension AddExpense : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    func openCamera()
    {
        
        let imagePicker = UIImagePickerController()
        
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let pickedImage = info[.editedImage] as? UIImage else {
            // imageViewPic.contentMode = .scaleToFill
            print("No image found")
            return
        }
        //journalImageView.image = pickedImage
        saveImage(image: pickedImage)
    }
    func openGallery()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true

        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    func saveImage(image : UIImage)
    {                _ = image.jpegData(compressionQuality: 0.50)
       // let id = UserDefaults.standard.string(forKey: userDefaultsKey.userMemberID)!
        //let json: [String: Any] = ["user_memberid" : "\(id)"]
        //profileImage = image
        expenseImage = image
        expenseImageView.image = image
        expenseImageView.contentMode = .scaleAspectFit
        
        //profileImageView.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 0)
        //uploadImage(image: imageData!, to: URL(string: "http://thefollo.com/housing/housing_android_api/Imagehelper/users_profileupload")!, params: json)
        
    }
}

protocol addExpenseProtocol {
    func refresh()
}


enum expenseType {
    case cash
    case card
}

