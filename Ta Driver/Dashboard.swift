//
//  Dashboard.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-19.
//

import MaterialComponents.MaterialBottomNavigation
import SystemConfiguration
import QuartzCore
import UIKit
import CoreLocation
import Alamofire
import CoreTelephony
import UIKit
import iCarousel
import CoreLocation
class Dashboard: UIViewController, iCarouselDelegate, iCarouselDataSource , CLLocationManagerDelegate, jobFinishedDelegate , deliveredJobsProtocol, MDCBottomNavigationBarDelegate, profileUpdateDelegate {
    func profileUpdated() {
        let defaults = UserDefaults.standard
        if defaults.string(forKey: "img") != nil
        {
            let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)driver/profile/\(defaults.string(forKey: "img")!)")
            print(urlStr)
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                
               if let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
               {
                DispatchQueue.main.async { [self] in
                    profileVIew.image = UIImage(data: data)
                    profileVIew.contentMode = .scaleAspectFill
                    profileVIew.layer.cornerRadius = 5
                  //  profileVIew.layer.cornerRadius = profileVIew.frame.width / 2
                   // profileVIew.layer.borderWidth = 5
                   // profileVIew.layer.borderColor = ConstantsUsedInProject.appThemeColor.cgColor
                   // profileVIew.elevate(elevation: 5)
                    //logoImageVIew.contentMode = .scaleAspectFit
                }
                   
                }
            }
        }
    }
    
    func pauseTimer() {
        //locationManager?.stopUpdatingLocation()
        t.suspend()
    }
    
    func resumeTimer() {
        //locationManager?.startUpdatingLocation()
        
        t.resume()
    }
    
    func toExpenses(data: Datum) {
        delivereJObDatum = data
        performSegue(withIdentifier: "toExpense", sender: nil)
        

    }
    
    func addExpense(data: Datum) {
        delivereJObDatum = data
        performSegue(withIdentifier: "deliverToAddExpense", sender: nil)
        delivereJObDatum = data
    }
    
    func toTrack(data: Datum) {
        delivereJObDatum = data
        performSegue(withIdentifier: "toTrack", sender: nil)
        delivereJObDatum = data
    }
    
    func refresh() {
        makePostCallAcceptd()
        makePostCallDashboardCount()
        
    }
    
    func jobDelivered() {
        dashSegment.selectedSegmentIndex = 3
        navigationBar.selectedItem = navigationBarItems[3]
        deliveredContainer.isHidden = false
        incompleteContiner.isHidden = true
        makePostCallDashboardCount()
        deliveredJob?.makePostCallDelivered()
        self.pauseTimer()
    }
    override func viewWillDisappear(_ animated: Bool) {
      //  super.viewWillDisappear(true)
      
       // self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false

   
    }
    override func viewWillAppear(_ animated: Bool) {
     //   super.viewWillAppear(true)
     //   self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
    //    self.navigationController?.navigationBar.shadowImage = UIImage()
     //   self.navigationController?.navigationBar.isTranslucent = true
     //   self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.isNavigationBarHidden = true

    }
    var address = ""

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dashBoardTable: UITableView!
    var delivereJObDatum : Datum?
    @IBOutlet weak var deliveredContainer: UIView!
    var assignedJobData : AssignedJobJSON?
    @IBOutlet weak var carouselView: iCarousel!
    @IBOutlet weak var incompleteContiner: UIView!
    var selectedType : jobType = .all
    var selectedJob = 0
    var time = 3
    var flag = 0
    var kilFlag = 0
    @IBOutlet weak var dashSegment: UISegmentedControl!
    var locationManager: CLLocationManager?
    var refreshControl = UIRefreshControl()

    var currentLocation : CLLocation?
    var previousLocation : CLLocation?
    var dashboardCountData : DashboardCountJSON?
    var timer = Timer()
    let t = RepeatingTimer(timeInterval: 10)
    var deliveredJob : DeliveredJobs?
    private var observer: NSObjectProtocol?
    
    @IBOutlet weak var profileVIew: UIImageView!
    
    @IBOutlet weak var navigationBar: MDCBottomNavigationBar!
    var navigationBarItems = [UITabBarItem]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
       
        carouselView.delegate = self
        carouselView.dataSource = self
        carouselView.type = .rotary
        carouselView.reloadData()
        let name = UserDefaults.standard.string(forKey: "name")
        nameLabel.text = name

        self.title = name
        t.resume()
     /*   if UserDefaults.standard.string(forKey: userdefaultsKey.jobSelected) == "true"
        {
            resumeTimer()
        }
        else
        {
            pauseTimer()
        } */
        t.eventHandler = { [self] in
            print("Timer Fired")
            time = time + 3
            print(Date())
            flag = 1
            locationManager?.requestLocation()
            prepareForUpload()
            if currentLocation != nil
            {
                getAdressName(coords: currentLocation!)
            }
            //   print(UIApplication.shared.backgroundTimeRemaining)
        }
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestAlwaysAuthorization()
        //locationManager?.allowsBackgroundLocationUpdates = true
        //locationManager?.requestLocation()
        locationManager?.startUpdatingLocation()
        startMySignificantLocationChanges()
        makePostCallDashboardCount()
      //  locationManager?.stopUpdatingLocation()
       // locationManager?.startMonitoringSignificantLocationChanges()
        dashBoardTable.delegate = self
        dashBoardTable.dataSource = self
        dashBoardTable.register(UINib(nibName: "DashboardTableCell", bundle: nil) , forCellReuseIdentifier: "DashboardTableCell")
        dashBoardTable.register(UINib(nibName: "DeliveredTableViewCell", bundle: nil) , forCellReuseIdentifier: "DeliveredTableViewCell")
     //   dashSegment.setImage(UIImage.textEmbeded(image: i, string: "dd", isImageBeforeText: true), forSegmentAt: 0)
        dashSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        dashSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: ConstantsUsedInProject.appThemeColor], for: .normal)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
           refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
           dashBoardTable.addSubview(refreshControl)
        dashBoardTable.backgroundView = nil
        dashBoardTable.isOpaque = false
        dashBoardTable.backgroundColor = #colorLiteral(red: 0.9332516193, green: 0.9333856702, blue: 0.9332222342, alpha: 1)
        dashSegment.apportionsSegmentWidthsByContent = true
        
        self.dashBoardTable.tableFooterView = UIView()
        dashBoardTable.separatorStyle = .none

        NotificationCenter.default.addObserver(self, selector: #selector(receivedNotification(notfication:)), name: NSNotification.Name(rawValue: "\(notificationNames.updateDashboard)"), object: nil)
        observer = NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: .main) { [unowned self] notification in
                    // do whatever you want when the app is brought back to the foreground
            print("foreground")
            segmentChnaged(dashSegment)
            makePostCallDashboardCount()
          
                }
     
        
       // bottomNavBar.titleVisibility = MDCBottomNavigationBarTitleVisibilitySelected
     //   bottomNavBar.alignment = MDCBottomNavigationBarAlignmentJustifiedAdjacentTitles

        let allItem = UITabBarItem(
            title: """
                ALL
                 ()
                """,
            image: UIImage(named:"all"),
            tag: 0)
        navigationBarItems.append(allItem)
        let assignedItem = UITabBarItem(
            title: """
                ASSIGNED
                 ()
                """,
            image: UIImage(named:"assigned"),
            tag: 1)
      //  let viewTabBar = assignedItem.value(forKey: "view") as? UIView
     //   let label = viewTabBar?.subviews[1] as? UILabel
  //      label?.setLineSpacing(lineSpacing: 1, lineHeightMultiple: 1)
        //assignedItem.badgeValue = "8"
        navigationBarItems.append(assignedItem)

        let acceptedItem = UITabBarItem(
            title: """
                IN TRANSIT
                 ()
                """,
            
            image: UIImage(named:"accepted"),
            tag: 2)
        navigationBarItems.append(acceptedItem)

        //acceptedItem.badgeValue = ""
        let deliveredItem = UITabBarItem(
            title: """
                DELIVERED
                 ()
                """,
            image: UIImage(named:"done"),
            tag: 3)
        navigationBarItems.append(deliveredItem)
        
       // deliveredItem.badgeValue = "88"

        let incompleteItem = UITabBarItem(
            title: """
                INCOMPLETE
                 ()
                """,
            image: UIImage(named:"incomplete"),
            tag: 4)
        navigationBarItems.append(incompleteItem)

        //incompleteItem.badgeValue = "888+"
       navigationBar.items = [allItem, assignedItem, acceptedItem, deliveredItem, incompleteItem]
        navigationBar.selectedItem = allItem
        navigationBar.delegate = self
        navigationBar.selectedItemTintColor = ConstantsUsedInProject.appThemeColor
        navigationBar.selectedItemTitleColor = ConstantsUsedInProject.appThemeColor
        navigationBar.tintColor = .darkGray
        navigationBar.titlesNumberOfLines = 2
        navigationBar.titleVisibility = MDCBottomNavigationBarTitleVisibility.always
        navigationBar.elevation = ShadowElevation(0)
        navigationBar.itemsContentVerticalMargin = 5
    
     //   profileVIew.layer.cornerRadius = profileVIew.frame.width / 2
        let defaults = UserDefaults.standard
        print(defaults.string(forKey: "id") as Any)
        if defaults.string(forKey: "img") != nil
        {
            let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)driver/profile/\(defaults.string(forKey: "img")!)")
            print(urlStr)
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                
               if let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
               {
                DispatchQueue.main.async { [self] in
                    profileVIew.image = UIImage(data: data)
                    profileVIew.contentMode = .scaleAspectFill
                    profileVIew.layer.cornerRadius = 5
                  //  profileVIew.layer.cornerRadius = profileVIew.frame.width / 2
                   // profileVIew.layer.borderWidth = 5
                   // profileVIew.layer.borderColor = ConstantsUsedInProject.appThemeColor.cgColor
                   // profileVIew.elevate(elevation: 5)
                    //logoImageVIew.contentMode = .scaleAspectFit
                }
                   
                }
            }
        }
        navigationBar.itemTitleFont = UIFont.systemFont(ofSize: 10)
        navigationBar.enableRippleBehavior = false
       /* if let n = UserDefaults.standard.string(forKey: "notification")
        {
            if n == "TRUE"
            {
                print("notification")
                notificationRecived()
            }
            else
            {
                makePostCallAll()

            }
        }
        else
        {
            makePostCallAll()

        } */
        makePostCallAll()
        let x = UserDefaults.standard.string(forKey: "loc")
        print("fbdhajdah")
        print(x as Any)
        print("dkuahkdhak")
        UserDefaults.standard.setValue("FALSE", forKey: "notification")
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    locationManager?.requestAlwaysAuthorization()

            case .authorizedAlways, .authorizedWhenInUse:
                    print("Access")
                @unknown default:
                break
            }
            } else {
                let alert = UIAlertController(title: "Expense", message: "Please turn on location", preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)        }
    
    }
    @IBAction func profileClicked(_ sender: Any) {
        performSegue(withIdentifier: "toProfile", sender: nil)
        
    }
    
    func bottomNavigationBar(_ bottomNavigationBar: MDCBottomNavigationBar, didSelect item: UITabBarItem) {
        if item.tag == 0
        {
            selectedType = .all
            makePostCallAll()
            deliveredContainer.isHidden = true
            incompleteContiner.isHidden = true
        }
        else
        if item.tag == 1
        {
            selectedType = .assigned
            makePostCall()
            deliveredContainer.isHidden = true
            incompleteContiner.isHidden = true

        }
        
        else
        if item.tag == 2

        {
            selectedType = .accepted
            makePostCallAcceptd()
            deliveredContainer.isHidden = true
            incompleteContiner.isHidden = true

        }
        else
        if item.tag == 3

        {
           
            deliveredContainer.isHidden = false
            incompleteContiner.isHidden = true

        }
        else
        if item.tag == 4

        {
           
            deliveredContainer.isHidden = true
            incompleteContiner.isHidden = false

        }
makePostCallDashboardCount()
        
    }
    func notificationRecived()
    {
        dashSegment.selectedSegmentIndex = 1
        navigationBar.selectedItem = navigationBarItems[1]

        selectedType = .assigned
        makePostCall()
        deliveredContainer.isHidden = true
        incompleteContiner.isHidden = true
makePostCallDashboardCount()
        UserDefaults.standard.setValue("FALSE", forKey: "notification")

        
    }
    func startMySignificantLocationChanges() {
        if !CLLocationManager.significantLocationChangeMonitoringAvailable() {
            
            let alert = UIAlertController(title: "Alert", message: "SignificantLocationChanges not supported", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            })
            alert.addAction(ok)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
            return
        }
        else
        {
            print("supported")
        }
        print("signif")
        //locationManager!.startMonitoringSignificantLocationChanges()
    }
    @objc func receivedNotification(notfication: NSNotification) {
        dashSegment.selectedSegmentIndex = 1
        selectedType = .assigned
        navigationBar.selectedItem = navigationBarItems[1]

        makePostCall()
        deliveredContainer.isHidden = true
        incompleteContiner.isHidden = true
makePostCallDashboardCount()
        UserDefaults.standard.setValue("FALSE", forKey: "notification")

    }

    @objc func refresh(_ sender: AnyObject) {
        refreshControl.endRefreshing()
        if selectedType == .all
        {
          
            makePostCallAll()
            deliveredContainer.isHidden = true
            incompleteContiner.isHidden = true
        }
        else
        if selectedType == .assigned
        {
            makePostCall()
            deliveredContainer.isHidden = true
            incompleteContiner.isHidden = true

        }
        
        else
        if selectedType == .accepted

        {
            selectedType = .accepted
            makePostCallAcceptd()
            deliveredContainer.isHidden = true
            incompleteContiner.isHidden = true

        }
       // segmentChnaged(dashSegment)
makePostCallDashboardCount()
        //makePostCallDelivered()
       // Code to refresh table view
    }
   
    func refreshSegmentControl()
    {
    dashSegment.setTitle("ALL (\(dashboardCountData?.dashboard?.total ?? "0"))", forSegmentAt: 0)
        dashSegment.setTitle("ASSIGNED (\(dashboardCountData?.dashboard?.assigned ?? "0"))", forSegmentAt: 1)
        dashSegment.setTitle("ACCEPTED (\(dashboardCountData?.dashboard?.accepted ?? "0"))", forSegmentAt: 2)
        dashSegment.setTitle("DELIVERED (\(dashboardCountData?.dashboard?.delivery ?? "0"))", forSegmentAt: 3)
        dashSegment.setTitle("INCOMPLETE (\(dashboardCountData?.dashboard?.incomplte ?? "0"))", forSegmentAt: 4)

        dashSegment.apportionsSegmentWidthsByContent = true
        //navigationBarItems.removeAll()
      
        navigationBarItems[0].title =  """
                ALL
                 (\(dashboardCountData?.dashboard?.total ?? "0"))
                """

        navigationBarItems[1].title = """
                ASSIGNED
                 (\(dashboardCountData?.dashboard?.assigned ?? "0"))
                """

        navigationBarItems[2].title = """
                IN TRANSIT
                 (\(dashboardCountData?.dashboard?.accepted ?? "0"))
                """

        navigationBarItems[3].title = """
                DELIVERED
                 (\(dashboardCountData?.dashboard?.delivery ?? "0"))
                """


        navigationBarItems[4].title = """
                INCOMPLETE
                 (\(dashboardCountData?.dashboard?.incomplte ?? "0"))
                """

      // navigationBar.items = [allItem, assignedItem, acceptedItem, deliveredItem, incompleteItem]
        

    }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MMM-yyyy"
            return  dateFormatter.string(from: date!)

        }
    func getAdressName(coords: CLLocation) {

        CLGeocoder().reverseGeocodeLocation(coords) { (placemark, error) in
                if error != nil {
                    print("Hay un error")
                } else {

                    let place = placemark! as [CLPlacemark]
                    if place.count > 0 {
                        let place = placemark![0]
                        var adressString : String = ""
                        if place.thoroughfare != nil {
                            adressString = adressString + place.thoroughfare! + ", "
                        }
                        if place.subThoroughfare != nil {
                            adressString = adressString + place.subThoroughfare! + ", "
                        }
                        if place.locality != nil {
                            adressString = adressString + place.locality! + " - "
                        }
                        if place.postalCode != nil {
                            adressString = adressString + place.postalCode! + ", "
                        }
                        if place.subAdministrativeArea != nil {
                            adressString = adressString + place.subAdministrativeArea! + " - "
                        }
                        if place.country != nil {
                            adressString = adressString + place.country!
                        }
                        self.address = adressString
                        print(self.address)
                        UserDefaults.standard.setValue("\(adressString)", forKey: "\(userdefaultsKey.currentAddress)")

                    }
                }
            }
      }

    func getConnectionType() -> String {
        guard let reachability = SCNetworkReachabilityCreateWithName(kCFAllocatorDefault, "www.google.com") else {
            return "NO INTERNET"
        }
        
        var flags = SCNetworkReachabilityFlags()
        SCNetworkReachabilityGetFlags(reachability, &flags)
        
        let isReachable = flags.contains(.reachable)
        let isWWAN = flags.contains(.isWWAN)
        
        if isReachable {
            if isWWAN {
                let networkInfo = CTTelephonyNetworkInfo()
                let carrierType = networkInfo.serviceCurrentRadioAccessTechnology
                
                guard let carrierTypeName = carrierType?.first?.value else {
                    return "UNKNOWN"
                }
                
                switch carrierTypeName {
                case CTRadioAccessTechnologyGPRS, CTRadioAccessTechnologyEdge, CTRadioAccessTechnologyCDMA1x:
                    return "2G"
                case CTRadioAccessTechnologyLTE:
                    return "4G"
                default:
                    return "3G"
                }
            } else {
                return "WIFI"
            }
        } else {
            return "NO INTERNET"
        }
    }
    func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
    func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }

    func getBearingBetweenTwoPoints1(point1 : CLLocation, point2 : CLLocation) -> (Double , Double) {

        let lat1 = degreesToRadians(degrees: point1.coordinate.latitude)
        let lon1 = degreesToRadians(degrees: point1.coordinate.longitude)

        let lat2 = degreesToRadians(degrees: point2.coordinate.latitude)
        let lon2 = degreesToRadians(degrees: point2.coordinate.longitude)

        let dLon = lon2 - lon1

        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)
        //print(radiansBearing)
        return (radiansToDegrees(radians: radiansBearing) , radiansBearing)
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)

    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

      /*  if let location = locations.last {
            
            print("New speed is \(location.speed)")
            print("New latitude is \(location.coordinate.latitude)")
            print("New long is \(location.coordinate.longitude)")
            let  lat = String((location.coordinate.latitude))
            let long = String((location.coordinate.longitude))
            let speed = String((location.speed))
            let speedAccur = String((location.speedAccuracy))
            let accur = String((location.horizontalAccuracy))
            prepareForUploadBackground(latitude: lat, longitude: long, speed: speed, speedaccur: speedAccur, accuracy: accur)
         
 
        } */
   /*     let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let defaultTimeZoneStr = formatter.string(from: date as Date)
        var x = UserDefaults.standard.string(forKey: "loc")

        UserDefaults.standard.setValue("\(x ?? "") ,\(defaultTimeZoneStr)", forKey: "loc")
         x = UserDefaults.standard.string(forKey: "loc")
         print(x)*/
        
        if flag == 1
        {
            //flag = 0
            if UIApplication.shared.applicationState  == .active
            {
                print("active")
                if let location = locations.last {
                    if currentLocation == nil
                    {
                        currentLocation = location
                        previousLocation = currentLocation
                    }
                    else
                    {
                        previousLocation = currentLocation
                        currentLocation = location
                        
                    }
                   // getAdressName(coords: location)

                    let distance = currentLocation?.distance(from: previousLocation!)
                    
                    print(Double(distance!) / 1000)
                    print("New speed is \(location.speed)")
                    print("New latitude is \(location.coordinate.latitude)")
                    print("New long is \(location.coordinate.longitude)")
                    //prepareForUpload()
                }
            }
            if UIApplication.shared.applicationState == .active {
                print("da")
            } else {
                print("App is backgrounded. New location is %@", locations.last as Any)
                print(locations.first as Any)
                if let location = locations.last {
                    if currentLocation == nil
                    {
                        currentLocation = location
                        previousLocation = currentLocation
                    }
                    else
                    {
                        previousLocation = currentLocation
                        currentLocation = location
                        
                    }
                    //getAdressName(coords: location)

                    _ = currentLocation?.distance(from: previousLocation!)
                    
                    print("New speed is \(location.speed)")
                    print("New latitude is \(location.coordinate.latitude)")
                    print("New long is \(location.coordinate.longitude)")
                    prepareForUpload()
               //     getAddressFromLatLon(pdblLatitude: String(location.coordinate.latitude), withLongitude: String(location.coordinate.longitude) , speed: String(location.speed) , accuracy: location.horizontalAccuracy, distance: String(distance!))
                }
            }
            
        }
   
    
            
        
    
        
    }
    func prepareForUploadBackground(latitude : String , longitude : String , speed : String , speedaccur : String , accuracy : String)
    {
        UIDevice.current.isBatteryMonitoringEnabled = true
        print(UIDevice.current.isBatteryMonitoringEnabled)
        var level = UIDevice.current.batteryLevel
        print(level*100)
        if level == -1.0
        {
            level = 0.00
        }
        let version = UIDevice.current.systemVersion
        print(version)
        let name = UIDevice.current.name
        print(name)
        let model = UIDevice.current.model
        print(model)
        let state = UIDevice.current.batteryState
        var batteryType = "discharging"
        switch state {
        case .charging:
            print("charging")
        case .full:
            batteryType = "full"
            
        case .unplugged:
            batteryType = "discharging"
            
        default:
            print("else")
        }
        print(getConnectionType())
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        
        // Get carrier name
        var carrierName = carrier?.carrierName
        if carrierName == nil
        {
            carrierName = ""
        }
       
        _ = UserDefaults.standard.string(forKey: "id")
        print("hhh")
        //let x = jobData!
        let lat = latitude
        let long = longitude
        let speed = speed
        let speedAccur = speedaccur
        let accur = accuracy
        let bearing = ""
        let bearingDegree = ""
     /*   if currentLocation != nil{
            lat = String((currentLocation?.coordinate.latitude)!)
            long = String((currentLocation?.coordinate.longitude)!)
            speed = String((currentLocation?.speed)!)
            speedAccur = String((currentLocation?.speedAccuracy)!)
            accur = String((currentLocation?.horizontalAccuracy)!)
            if previousLocation != nil
            {
                let y = getBearingBetweenTwoPoints1(point1: currentLocation!, point2: previousLocation!)
                bearingDegree = String(y.0)
                bearing = String(y.1)
                print(bearing)
                
            }

        }
        else
        {
            if  locationManager?.location?.coordinate.latitude != nil
            {
                lat = String((locationManager?.location?.coordinate.latitude)!)
                long = String((locationManager?.location?.coordinate.longitude)!)
                speed = String((locationManager?.location?.speed)!)
                speedAccur = String((locationManager?.location?.speedAccuracy)!)
                accur = String((locationManager?.location?.horizontalAccuracy)!)

            }
        }  */
        
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    //    formatter.timeZone = TimeZone(abbreviation: "UTC")
        let defaultTimeZoneStr = formatter.string(from: date as Date)
   
        let defaults = UserDefaults.standard
        
        if UserDefaults.standard.string(forKey: userdefaultsKey.jobSelected) == "true"
        {
        let json: [String: Any] = ["sign_emp_id" : "","sign_emp_name" : "" , "trackid" : "","ta_jobid" : "\(defaults.string(forKey: userdefaultsKey.jobid)!)" , "userid" : "\(defaults.string(forKey: userdefaultsKey.userID)!)" , "ta_form_id" : "\(defaults.string(forKey: userdefaultsKey.taformid)!)" , "clientid" : "\(defaults.string(forKey: userdefaultsKey.cliendid)!)" , "driverid" : "\(defaults.string(forKey: userdefaultsKey.driverID)!)" , "jobstatus" : "\(defaults.string(forKey: userdefaultsKey.jobStatus)!)" , "description" : "" , "trackdatetime" : "\(defaultTimeZoneStr)" , "currentlocation" : "" ,"destinationlocation" : "0,0" , "pickuplocation" : "0,0" , "latitude" : "\(lat)" , "longitude" : "\(long)" , "status" : "active" , "createby" : "\(defaults.string(forKey: userdefaultsKey.name)!)", "modifyby" : "\(defaults.string(forKey: userdefaultsKey.name)!)" , "modifydate" : "\(defaultTimeZoneStr)" ,"deviceid" : "" , "bearing" : "\(bearing)" , "bearingaccuracydegrees" : "\(bearingDegree)" , "speed" : "\(speed)" , "speedaccuracymeterspersecond" : "\(speedAccur)" , "accuracy" : "\(accur)" , "jobnumber" : "\(defaults.string(forKey: userdefaultsKey.jobNumber)!)" , "battery_type" : "\(batteryType)" , "battery_percentage" : "\(Int(level*100))" , "network_type" : "\(getConnectionType())" , "network_operator" : "\(carrierName ?? "")" , "geofence" : "EXIT Warehouse" , "support_driver_flag" : "" ,  "epod_job_id" : "\(defaults.string(forKey: userdefaultsKey.epodJobid)!)" , "main_taform_id" : "\(defaults.string(forKey: userdefaultsKey.mainTaFormID)!)"]
         //   let imageData = fromAttachImage!.jpegData(compressionQuality: 0.50)
            print(address)
            uploadImage(to: URL(string: "\(ConstantsUsedInProject.baseUrl)api/addtrackwithflag")!, params: json)
        print(json)
        }
        else
        {
            let json: [String: Any] = ["sign_emp_id" : "","sign_emp_name" : "" , "trackid" : "","ta_jobid" : "" , "userid" : "" , "ta_form_id" : "" , "clientid" : "0" , "driverid" : "\(defaults.string(forKey: userdefaultsKey.driverID)!)" , "jobstatus" : "IDLE" , "description" : "" , "trackdatetime" : "\(defaultTimeZoneStr)" , "currentlocation" : "" ,"destinationlocation" : "0,0" , "pickuplocation" : "0,0" , "latitude" : "\(lat)" , "longitude" : "\(long)" , "status" : "active" , "createby" : "\(defaults.string(forKey: userdefaultsKey.name)!)", "modifyby" : "\(defaults.string(forKey: userdefaultsKey.name)!)" , "modifydate" : "\(defaultTimeZoneStr)" ,"deviceid" : "" , "bearing" : "\(bearing)" , "bearingaccuracydegrees" : "\(bearingDegree)" , "speed" : "\(speed)" , "speedaccuracymeterspersecond" : "\(speedAccur)" , "accuracy" : "\(accur)" , "jobnumber" : "0" , "battery_type" : "\(batteryType)" , "battery_percentage" : "\(Int(level*100))" , "network_type" : "\(getConnectionType())" , "network_operator" : "\(carrierName ?? "")" , "geofence" : "EXIT Warehouse" , "support_driver_flag" : "" ,  "epod_job_id" : "" , "main_taform_id" : ""]
             //   let imageData = fromAttachImage!.jpegData(compressionQuality: 0.50)

                uploadImage(to: URL(string: "\(ConstantsUsedInProject.baseUrl)api/addtrackwithflag")!, params: json)
        }
        

    }
    @IBAction func logoutPressed(_ sender: Any) {
      
        
        let alert = UIAlertController(title: "LogOut", message: "Are You Sure You Want To Sign Out", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: { [self] action in
            let defaults = UserDefaults.standard
            defaults.setValue("false", forKey: "\(userdefaultsKey.jobSelected)")
            t.suspend()
            defaults.setValue("FALSE", forKey: "loggedIn")
            pauseTimer()
            locationManager?.stopUpdatingLocation()
            let storyboard = UIStoryboard(name: "Login", bundle: nil)

            let dashboard1 = storyboard.instantiateViewController(identifier: "login")
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(dashboard1)
        })
        alert.addAction(ok)
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
        })
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })

    }
    func prepareForUpload()
    {
        UIDevice.current.isBatteryMonitoringEnabled = true
        print(UIDevice.current.isBatteryMonitoringEnabled)
        var level = UIDevice.current.batteryLevel
        print(level*100)
        if level == -1.0
        {
            level = 0.00
        }
        let version = UIDevice.current.systemVersion
        print(version)
        let name = UIDevice.current.name
        print(name)
        let model = UIDevice.current.model
        print(model)
        let state = UIDevice.current.batteryState
        var batteryType = "discharging"
        switch state {
        case .charging:
            print("charging")
        case .full:
            batteryType = "full"
            
        case .unplugged:
            batteryType = "discharging"
            
        default:
            print("else")
        }
        print(getConnectionType())
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        
        // Get carrier name
        var carrierName = carrier?.carrierName
        if carrierName == nil
        {
            carrierName = ""
        }
       
        _ = UserDefaults.standard.string(forKey: "id")
        print("hhh")
        //let x = jobData!
        var lat = ""
        var long = ""
        var speed = ""
        var speedAccur = ""
        var accur = ""
        var bearing = ""
        var bearingDegree = ""
        if currentLocation != nil{
            lat = String((currentLocation?.coordinate.latitude)!)
            long = String((currentLocation?.coordinate.longitude)!)
            speed = String((currentLocation?.speed)!)
            speedAccur = String((currentLocation?.speedAccuracy)!)
            accur = String((currentLocation?.horizontalAccuracy)!)
            if previousLocation != nil
            {
                let y = getBearingBetweenTwoPoints1(point1: currentLocation!, point2: previousLocation!)
                bearingDegree = String(y.0)
                bearing = String(y.1)
                print(bearing)
                
            }

        }
        else
        {
            if  locationManager?.location?.coordinate.latitude != nil
            {
                lat = String((locationManager?.location?.coordinate.latitude)!)
                long = String((locationManager?.location?.coordinate.longitude)!)
                speed = String((locationManager?.location?.speed)!)
                speedAccur = String((locationManager?.location?.speedAccuracy)!)
                accur = String((locationManager?.location?.horizontalAccuracy)!)

            }
        }
        
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    //    formatter.timeZone = TimeZone(abbreviation: "UTC")
        let defaultTimeZoneStr = formatter.string(from: date as Date)
   
        let defaults = UserDefaults.standard
        
        if UserDefaults.standard.string(forKey: userdefaultsKey.jobSelected) == "true"
        {
        let json: [String: Any] = ["sign_emp_id" : "","sign_emp_name" : "" , "trackid" : "","ta_jobid" : "\(defaults.string(forKey: userdefaultsKey.jobid)!)" , "userid" : "\(defaults.string(forKey: userdefaultsKey.userID)!)" , "ta_form_id" : "\(defaults.string(forKey: userdefaultsKey.taformid)!)" , "clientid" : "\(defaults.string(forKey: userdefaultsKey.cliendid)!)" , "driverid" : "\(defaults.string(forKey: userdefaultsKey.driverID)!)" , "jobstatus" : "\(defaults.string(forKey: userdefaultsKey.jobStatus)!)" , "description" : "" , "trackdatetime" : "\(defaultTimeZoneStr)" , "currentlocation" : "\(address)" ,"destinationlocation" : "0,0" , "pickuplocation" : "0,0" , "latitude" : "\(lat)" , "longitude" : "\(long)" , "status" : "active" , "createby" : "\(defaults.string(forKey: userdefaultsKey.name)!)", "modifyby" : "\(defaults.string(forKey: userdefaultsKey.name)!)" , "modifydate" : "\(defaultTimeZoneStr)" ,"deviceid" : "" , "bearing" : "\(bearing)" , "bearingaccuracydegrees" : "\(bearingDegree)" , "speed" : "\(speed)" , "speedaccuracymeterspersecond" : "\(speedAccur)" , "accuracy" : "\(accur)" , "jobnumber" : "\(defaults.string(forKey: userdefaultsKey.jobNumber)!)" , "battery_type" : "\(batteryType)" , "battery_percentage" : "\(Int(level*100))" , "network_type" : "\(getConnectionType())" , "network_operator" : "\(carrierName ?? "")" , "geofence" : "EXIT Warehouse" , "support_driver_flag" : "" ,  "epod_job_id" : "\(defaults.string(forKey: userdefaultsKey.epodJobid)!)" , "main_taform_id" : "\(defaults.string(forKey: userdefaultsKey.mainTaFormID)!)"]
         //   let imageData = fromAttachImage!.jpegData(compressionQuality: 0.50)
            print(address)
            uploadImage(to: URL(string: "\(ConstantsUsedInProject.baseUrl)api/addtrackwithflag")!, params: json)
        print(json)
        }
        else
        {
            let json: [String: Any] = ["sign_emp_id" : "","sign_emp_name" : "" , "trackid" : "","ta_jobid" : "" , "userid" : "" , "ta_form_id" : "" , "clientid" : "0" , "driverid" : "\(defaults.string(forKey: userdefaultsKey.driverID)!)" , "jobstatus" : "IDLE" , "description" : "" , "trackdatetime" : "\(defaultTimeZoneStr)" , "currentlocation" : "\(address)" ,"destinationlocation" : "0,0" , "pickuplocation" : "0,0" , "latitude" : "\(lat)" , "longitude" : "\(long)" , "status" : "active" , "createby" : "\(defaults.string(forKey: userdefaultsKey.name)!)", "modifyby" : "\(defaults.string(forKey: userdefaultsKey.name)!)" , "modifydate" : "\(defaultTimeZoneStr)" ,"deviceid" : "" , "bearing" : "\(bearing)" , "bearingaccuracydegrees" : "\(bearingDegree)" , "speed" : "\(speed)" , "speedaccuracymeterspersecond" : "\(speedAccur)" , "accuracy" : "\(accur)" , "jobnumber" : "0" , "battery_type" : "\(batteryType)" , "battery_percentage" : "\(Int(level*100))" , "network_type" : "\(getConnectionType())" , "network_operator" : "\(carrierName ?? "")" , "geofence" : "EXIT Warehouse" , "support_driver_flag" : "" ,  "epod_job_id" : "" , "main_taform_id" : ""]
             //   let imageData = fromAttachImage!.jpegData(compressionQuality: 0.50)

                uploadImage(to: URL(string: "\(ConstantsUsedInProject.baseUrl)api/addtrackwithflag")!, params: json)
            print(json)

        }
        

    }
    func uploadImage( to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                /*  if let data = value.data(using: .utf8) {
                      multipart.append(data, withName: key)
                      print(String(data: data, encoding: String.Encoding.utf8) as Any)

                  } */
                  if let data = value.removingPercentEncoding?.data(using: .utf8) {
                     // do your stuff here
                      multipart.append(data, withName: key)
                      //print(String(data: data, encoding: String.Encoding.utf8) as Any)
                  }
                              }

            //multipart.append(image, withName: "attachmentimage", fileName: "profile", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {

        
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(JobDepartureDeliveryJSON.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                        
                        DispatchQueue.main.async { [self] in
                            
                            if code_str == 200 {
                               print("success")
                          
                                print("dashboard")
                                
                            }
                            else
                            {
                              
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }

  
    func numberOfItems(in carousel: iCarousel) -> Int {
        return assignedJobData?.data?.count ?? 0
    }
    
    @IBAction func segmentChnaged(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0
        {
            selectedType = .all
            makePostCallAll()
            deliveredContainer.isHidden = true
            incompleteContiner.isHidden = true
        }
        else
        if sender.selectedSegmentIndex == 1
        {
            selectedType = .assigned
            makePostCall()
            deliveredContainer.isHidden = true
            incompleteContiner.isHidden = true

        }
        
        else
        if sender.selectedSegmentIndex == 2

        {
            selectedType = .accepted
            makePostCallAcceptd()
            deliveredContainer.isHidden = true
            incompleteContiner.isHidden = true

        }
        else
        if sender.selectedSegmentIndex == 3

        {
           
            deliveredContainer.isHidden = false
            incompleteContiner.isHidden = true

        }
        else
        if sender.selectedSegmentIndex == 4

        {
           
            deliveredContainer.isHidden = true
            incompleteContiner.isHidden = false

        }
makePostCallDashboardCount()
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let frame  = CGRect(x: 0, y: 0, width: 330, height: 400)
    
        let x = assignedJobData?.data?[index]
        let myView = DashboardEntries.instantiate(message: "\(index)")
        myView.frame = frame
        myView.x()
        let y = Double(x?.overnightTotalAmount ?? "0.0")
        let s = String(format: "%.2f", y ?? 0.0)
        myView.advance.text = "MYR \(s)"
        myView.date.text = convertDateFormater((x?.jobDate!)!)
        myView.comission.text = "MYR \(x?.jobCommision ?? "")"
        myView.taNumber.text = x?.taTadynamicNumber
        myView.from.text = x?.jobFrom?.uppercased()
        myView.to.text = x?.jobClientName?.uppercased()
        myView.jobNumbwe.text = x?.jobNumber
        //myView.frame = frame
        myView.acceptButton.tag = index
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (acceptPressed))  //Tap function will call when user tap on button
        myView.acceptButton.addGestureRecognizer(tapGesture)
        myView.segmentCOntrol.tag = index
        myView.segmentCOntrol.addTarget(self, action: #selector(segmentAction(_:)), for: .valueChanged)
        if selectedType == .accepted
        {
            myView.jobNmae.text = x?.jobStatus?.uppercased()

            myView.acceptButton.setTitle("VIEW JOB", for: .normal)
        }
        else
        {
            myView.jobNmae.text = "ASSIGNED \(index + 1)"

            myView.acceptButton.setTitle("ACCEPT", for: .normal)

        }

        return myView
    }
    @objc func segmentAction(_ segmentedControl: UISegmentedControl) {
        let i = segmentedControl.tag
           switch (segmentedControl.selectedSegmentIndex) {
           case 0:
            let c = carouselView.currentItemView as! DashboardEntries
            c.from.text = assignedJobData?.data?[i].jobFrom?.uppercased()
            print("1")
               break // Uno
           case 1:
            let c = carouselView.currentItemView as! DashboardEntries
            c.from.text = assignedJobData?.data?[i].jobTo?.uppercased()
            print("2")
               break // Dos
           case 2:
               break // Tres
           default:
               break
           }
       }
    @objc func acceptPressed(sender: UITapGestureRecognizer)
    {
        let i = sender.view?.tag
        if selectedType == .all
        {
            let x = assignedJobData?.data?[i!]
           
            if x?.status?.uppercased() == "ASSIGNED" //|| x?.status?.uppercased() == "DELIVERED"
            {
                makePostCallAccept(cliendId: x!.taFormID!, jobid: x!.jobID!, truckid: x!.truckNumberid!, dricerId: x!.taDriverID!, jobNumber: x!.jobNumber!, createBY: x!.createby! , jobprimaryid: x!.jobIdPrimary! , taformid: x!.taFormID! , tag: i!, mianTaFOrmId: x!.mainTaFormId!)

            }
            else
            {
                selectedJob = i!
                performSegue(withIdentifier: "toJob", sender: nil)
            }


        }
        else
        {
        if selectedType == .assigned
        {
        let x = assignedJobData?.data?[i!]
        print("selceted")
        if selectedType == .assigned {
            makePostCallAccept(cliendId: x!.taFormID!, jobid: x!.jobID!, truckid: x!.truckNumberid!, dricerId: x!.taDriverID!, jobNumber: x!.jobNumber!, createBY: x!.createby! , jobprimaryid: x!.jobIdPrimary! , taformid: x!.taFormID!, tag: i!, mianTaFOrmId: x!.mainTaFormId!)
            
        }
        }
        else
        {
            print(selectedJob)
            selectedJob = i!
            performSegue(withIdentifier: "toJob", sender: nil)
        }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toJob"
        {
            let x = assignedJobData?.data?[selectedJob]

            let vc = segue.destination as! JobDetails
            vc.jobData = x
            vc.delegate = self
            if UserDefaults.standard.string(forKey: userdefaultsKey.jobSelected) == "true"
            {
               // t.resume()
                print("running")
            }
            else
            {
                let defaults = UserDefaults.standard
                defaults.setValue("\(x!.taFormID!)", forKey: "\(userdefaultsKey.cliendid)")
                defaults.setValue("\(x!.taDriverID!)", forKey: "\(userdefaultsKey.driverID)")
                defaults.setValue("\(x!.jobStatus!)", forKey: "\(userdefaultsKey.jobStatus)")
                defaults.setValue("\(x!.jobID!)", forKey: "\(userdefaultsKey.jobid)")
                defaults.setValue("\(x!.taFormID! )", forKey: "\(userdefaultsKey.taformid)")
                defaults.setValue("\(x!.epodJobID!)", forKey: "\(userdefaultsKey.epodJobid)")
                defaults.setValue("\(x!.taFormID!)", forKey: "\(userdefaultsKey.userID)")
                defaults.setValue("\(x!.jobNumber!)", forKey: "\(userdefaultsKey.jobNumber)")
                defaults.setValue("\(x!.createby!)", forKey: "\(userdefaultsKey.createBy)")
                defaults.setValue("\(x!.mainTaFormId!)", forKey: "\(userdefaultsKey.mainTaFormID)")

                defaults.setValue("true", forKey: "\(userdefaultsKey.jobSelected)")
                resumeTimer()
                
            }

        }
        if segue.identifier == "delivered"
        {
            let vc = segue.destination as! DeliveredJobs
            vc.delegate = self
            deliveredJob = vc
        }
        if segue.identifier == "toExpense"
        {
            let x = delivereJObDatum

            let vc = segue.destination as! Expenses
            vc.jobData = x
        }
        if segue.identifier == "toTrack"
        {
            let x = delivereJObDatum
            let vc = segue.destination as! TrackJobDelivered
            vc.jobData = x
        }
        if segue.identifier == "deliverToAddExpense"
        {
            let x = delivereJObDatum
            let vc = segue.destination as! AddExpense
            vc.jobData = x
        }
        if segue.identifier == "incomplete"
        {
            let vc = segue.destination as! IncompleteJobs
            vc.delegate = self
          //  deliveredJob = vc
        }
        if segue.identifier == "toProfile"
        {
            let vc = segue.destination as! ProfileUpdate
            vc.delegate = self
        }
        
    }
    func makePostCallDashboardCount() {
      
       
        let id = UserDefaults.standard.string(forKey: "id")
        let decoder = JSONDecoder()
       
        let postString = "id=\(id!)&status=DASHBOARD"
        // create post request
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)job/driverjobcounts")
        {
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = postString.data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(DashboardCountJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                     //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    print(loginBaseResponse as Any)
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            
                            dashboardCountData = loginBaseResponse
                            refreshSegmentControl()
                            
                        }
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

    func makePostCallAll() {
      
       
        let id = UserDefaults.standard.string(forKey: "id")
        let decoder = JSONDecoder()
       
        let postString = "id=\(id!)&status=ALL"
        // create post request
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)job/driverjob")
        {
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = postString.data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                    let loginBaseResponse = try? decoder.decode(AssignedJobJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            
                          assignedJobData = nil
                            print(loginBaseResponse as Any)
                            assignedJobData = loginBaseResponse
                          //  carouselView.reloadData()
                            refreshSegmentControl()

                            dashBoardTable.reloadData()
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            assignedJobData = nil
                                assignedJobData = loginBaseResponse

                                dashBoardTable.reloadData()
                            let alert = UIAlertController(title: "DriverJob", message: "\(loginBaseResponse?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                         //   self.present(alert, animated: true, completion: nil)
                          
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    func makePostCall() {
      
       
        let id = UserDefaults.standard.string(forKey: "id")
        let decoder = JSONDecoder()
       
        let postString = "id=\(id!)&status=ASSIGNED"
        // create post request
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)job/driverjob")
        {
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = postString.data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(AssignedJobJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            
                          assignedJobData = nil
                            print(loginBaseResponse as Any)
                            assignedJobData = loginBaseResponse
                           // carouselView.reloadData()
                            dashBoardTable.reloadData()
                            refreshSegmentControl()
                           // navigationBar.selectedItem = navigationBarItems[1]


                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            assignedJobData = nil
                                assignedJobData = loginBaseResponse
                            refreshSegmentControl()

                                dashBoardTable.reloadData()
                            let alert = UIAlertController(title: "DriverJob", message: "\(loginBaseResponse?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                         //   self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    func makePostCallAcceptd() {
      
       
        let id = UserDefaults.standard.string(forKey: "id")
        let decoder = JSONDecoder()
       
        let postString = "id=\(id!)&status=ACCEPTED"
        // create post request
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)job/driverjob")
        {
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = postString.data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(AssignedJobJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            
                          assignedJobData = nil
                            print(loginBaseResponse as Any)
                            assignedJobData = loginBaseResponse
                          //  carouselView.reloadData()
                            dashBoardTable.reloadData()
                            refreshSegmentControl()

                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            print(loginBaseResponse as Any)
                        assignedJobData = nil
                            assignedJobData = loginBaseResponse

                            dashBoardTable.reloadData()
                            refreshSegmentControl()

                            let alert = UIAlertController(title: "DriverJob", message: "\(loginBaseResponse?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                         //   self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    func makePostCallAccept(cliendId : String , jobid : String , truckid : String , dricerId : String , jobNumber : String , createBY : String , jobprimaryid : String , taformid : String , tag : Int , mianTaFOrmId : String) {
      
       
        //let id = UserDefaults.standard.string(forKey: "id")
        let decoder = JSONDecoder()
       
        let postString = "epod_jobid=\(jobprimaryid)&status=ACCEPTED&jobid=\(jobid)&ta_form_id=\(taformid)&reason=&driverid=\(dricerId)"
        // create post request
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)api/acceptedstatus")
        {
            print(url)
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = postString.data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                     print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                    let loginBaseResponse = try? decoder.decode(AssignedJobJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            print(loginBaseResponse as Any)

                            let defaults = UserDefaults.standard
                            defaults.setValue("\(cliendId)", forKey: "\(userdefaultsKey.cliendid)")
                            defaults.setValue("\(dricerId)", forKey: "\(userdefaultsKey.driverID)")
                            defaults.setValue("COLLECTING", forKey: "\(userdefaultsKey.jobStatus)")
                            defaults.setValue("\(jobid)", forKey: "\(userdefaultsKey.jobid)")
                            defaults.setValue("\(loginBaseResponse?.data?[0].taFormID ?? "")", forKey: "\(userdefaultsKey.taformid)")
                            defaults.setValue("\(loginBaseResponse?.data?[0].epodJobID ?? "")", forKey: "\(userdefaultsKey.epodJobid)")
                            defaults.setValue("\(cliendId)", forKey: "\(userdefaultsKey.userID)")
                            defaults.setValue("\(jobNumber)", forKey: "\(userdefaultsKey.jobNumber)")
                            defaults.setValue("\(createBY)", forKey: "\(userdefaultsKey.createBy)")
                            defaults.setValue("\(mianTaFOrmId)", forKey: "\(userdefaultsKey.mainTaFormID)")

                            defaults.setValue("true", forKey: "\(userdefaultsKey.jobSelected)")
                            t.resume()
                           
                                selectedJob = tag
                            assignedJobData?.data?[selectedJob].jobStatus = "COLLECTING"
                                
                                dashSegment.selectedSegmentIndex = 2
                            navigationBar.selectedItem = navigationBarItems[2]
                                performSegue(withIdentifier: "toJob", sender: nil)

                                selectedType = .accepted
                                makePostCallAcceptd()
                                deliveredContainer.isHidden = true
                            incompleteContiner.isHidden = true
                                //refreshSegmentControl()
                                makePostCallDashboardCount()
                            
                            
                          
                            
                         // assignedJobData = nil
                            print(loginBaseResponse as Any)
                           // carouselView.reloadData()
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            print(loginBaseResponse as Any)

                            let alert = UIAlertController(title: "Job", message: "\(loginBaseResponse?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    @objc func trackjob(sender: UITapGestureRecognizer)
    {
        
        let i = sender.view?.tag

        selectedJob = i!
        let x = assignedJobData?.data?[selectedJob]
        toTrack(data: x!)
      //  performSegue(withIdentifier: "toTrack", sender: nil)
    }
    @objc func expensePressed(sender: UITapGestureRecognizer)
    {
        let i = sender.view?.tag

        selectedJob = i!
        let x = assignedJobData?.data?[selectedJob]
        toExpenses(data: x!)
      //  performSegue(withIdentifier: "toExpense", sender: nil)
    }

}

extension Dashboard : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if assignedJobData?.data?.count ?? 0 == 0 {
              self.dashBoardTable.setEmptyMessage("No Job Found")
          } else {
            self.dashBoardTable.restore()
          }

        return assignedJobData?.data?.count ?? 0
      //  return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let x = assignedJobData?.data?[indexPath.row]
        if x?.status?.uppercased() == "DELIVERED" || x?.status?.uppercased() == "INCOMPLETE"
        {
            
            let vendor = UserDefaults.standard.string(forKey: userdefaultsKey.vendorOptionDriver)?.uppercased()
            if vendor == "OTHER"
            {
                let cell = dashBoardTable.dequeueReusableCell(withIdentifier: "DashboardTableCell") as! DashboardTableCell
                
                let y = Double(x?.jobCommision ?? "0.0")
                let s = String(format: "%.2f", y ?? 0.0)
                cell.payToDriver.text = "MYR \(s)"
                cell.payToDriver.textColor = ConstantsUsedInProject.appThemeColor
                cell.date.text = convertDateFormater((x?.jobDate!)!)
                cell.commission.text = ""
                cell.commissionText.text = "Rental"
                cell.payToDriverText.text = ""
                cell.taNumber.text = x?.taTadynamicNumber
                cell.from.text = x?.shipperName?.uppercased()
                cell.to.text = x?.consigneeName?.uppercased()
                cell.jobNuber.text = x?.jobNumber
                cell.truckType.text = x?.jobChargeType
                cell.truckNumber.text = x?.truckNumber
              //  cell.companyName.text = x?.jobClientName
                cell.fromAddress.text = x?.shipperLocationaddress
                cell.toAddress.text = x?.consigneeLocationaddress
                cell.status.text = x?.status
                if x?.status?.uppercased() == "INCOMPLETE"
                    {
                    cell.buttonLabel.text = "TRACK"
                    cell.status.textColor = .darkGray
                
                    cell.buttonView.backgroundColor = .systemGray

                }
                else
                {
                    cell.buttonLabel.text = "TRACK"

                    cell.status.textColor = .systemGreen
                    cell.buttonView.backgroundColor = .systemGreen
                }
                cell.acceptButton.tag = indexPath.row
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector (trackjob(sender:)))
                cell.acceptButton.addGestureRecognizer(tapGesture)
                return cell
            }
             
            let cell = dashBoardTable.dequeueReusableCell(withIdentifier: "DeliveredTableViewCell") as! DeliveredTableViewCell
            let y = Double(x?.totalPaytodriver ?? "0.0")
            let s = String(format: "%.2f", y ?? 0.0)
            cell.payToDriver.text = "MYR \(s)"
            cell.date.text = convertDateFormater((x?.jobDate!)!)
            cell.commission.text = "MYR \(x?.jobCommision ?? "")"
            cell.taNumber.text = x?.taTadynamicNumber
            
            cell.jobNuber.text = x?.jobNumber
            cell.truckType.text = x?.jobChargeType
            cell.truckNumber.text = x?.truckNumber
      //      cell.companyName.text = x?.jobClientName
            cell.trackButton.tag = indexPath.row
            cell.edpenseBUtton.tag = indexPath.row
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector (expensePressed(sender:)))
            cell.edpenseBUtton.addGestureRecognizer(tapGesture)
            cell.edpenseBUtton.setBackgroundColor(#colorLiteral(red: 0.1450980392, green: 0.4941176471, blue: 0.1725490196, alpha: 1), forState: .highlighted)

            //myView.expenseListButton.tag = index
            //myView.addExpenseButton.tag = index
            //let addGesture = UITapGestureRecognizer(target: self, action: #selector (addExpense(sender:)))
            let trackGesture = UITapGestureRecognizer(target: self, action: #selector (trackjob(sender:)))
            cell.trackButton.addGestureRecognizer(trackGesture)
            cell.trackButton.setBackgroundColor(#colorLiteral(red: 0.1450980392, green: 0.4941176471, blue: 0.1725490196, alpha: 1), forState: .highlighted)

            cell.status.text = x?.status?.uppercased()
            cell.status.textColor = .systemGreen
            cell.from.text = x?.shipperName?.uppercased()
            cell.to.text = x?.consigneeName?.uppercased()
            cell.fromAddress.text = x?.shipperLocationaddress
            cell.toAddress.text = x?.consigneeLocationaddress
            if x?.status?.uppercased() == "INCOMPLETE"
                {
                cell.status.textColor = .darkGray
                cell.buttonView.backgroundColor = .systemGray
                cell.buttonView2.backgroundColor = .systemGray

            }
            else
            {
                cell.status.textColor = .systemGreen
                cell.buttonView.backgroundColor = .systemGreen
                cell.buttonView2.backgroundColor = .systemGreen
            }
            let c = check(modifyData: x!.modifydate!)
            cell.timeLeftLabel.text =  c.0
            if x?.operationStatus?.uppercased() == "VERIFIED" || c.1 == false
            {
                cell.timeLeftLabel.text = "For Expenses Contact To Admin".uppercased()
                cell.timeLeftLabel.textColor = .systemRed
            }
            else
            {
                cell.timeLeftLabel.textColor = .systemGreen

            }
            //myView.addExpenseButton.addGestureRecognizer(addGesture)
            return cell
        }
        let cell = dashBoardTable.dequeueReusableCell(withIdentifier: "DashboardTableCell") as! DashboardTableCell
        
        
        let y = Double(x?.totalPaytodriver ?? "0.0")
        let s = String(format: "%.2f", y ?? 0.0)
        cell.payToDriver.text = "MYR \(s)"
        cell.date.text = convertDateFormater((x?.jobDate!)!)
        cell.commission.text = "MYR \(x?.jobCommision ?? "")"
        cell.taNumber.text = x?.taTadynamicNumber
        cell.from.text = x?.shipperName?.uppercased()
        cell.to.text = x?.consigneeName?.uppercased()
        cell.jobNuber.text = x?.jobNumber
        cell.truckType.text = x?.jobChargeType
        cell.truckNumber.text = x?.truckNumber
      //  cell.companyName.text = x?.jobClientName
        cell.fromAddress.text = x?.shipperLocationaddress
        cell.toAddress.text = x?.consigneeLocationaddress
        let vendor = UserDefaults.standard.string(forKey: userdefaultsKey.vendorOptionDriver)?.uppercased()
        if vendor == "OTHER"
        {
            let y = Double(x?.jobCommision ?? "0.0")
            let s = String(format: "%.2f", y ?? 0.0)
            cell.payToDriver.text = "MYR \(s)"
            cell.payToDriver.textColor = ConstantsUsedInProject.appThemeColor
            cell.date.text = convertDateFormater((x?.jobDate!)!)
            cell.commission.text = ""
            cell.commissionText.text = "Rental"
            cell.payToDriverText.text = ""
        }
        if selectedType == .accepted
        {
            cell.status.text = x?.jobStatus?.uppercased()

            if x?.jobStatus == "ACCEPTED" // this is for when accepting job 
            {
                cell.status.text = "COLLECTING"
                
            }
            cell.buttonView.backgroundColor = .systemYellow
            cell.status.textColor = .systemYellow
            //cell.acceptButton.setTitle("VIEW JOB", for: .normal)
            cell.buttonLabel.text = "VIEW JOB"
        }
        else
        if selectedType == .assigned
            {
            cell.status.text = "ASSIGNED \(indexPath.row + 1)"

            cell.buttonLabel.text = "ACCEPT"
            cell.buttonView.backgroundColor = .systemRed
            cell.status.textColor = .systemRed
        }
        else
        {// all
            if x?.status?.uppercased() == "ASSIGNED" //|| x?.status?.uppercased() == "DELIVERED"
            {
                cell.buttonLabel.text = "ACCEPT"
                cell.status.text = x?.status?.uppercased()
                cell.buttonView.backgroundColor = .systemRed
                cell.status.textColor = .systemRed

            }
            else
            {
            cell.buttonLabel.text = "VIEW JOB"
                cell.status.text = x?.jobStatus?.uppercased()
                cell.buttonView.backgroundColor = .systemYellow
                cell.status.textColor = .systemYellow

            }
        }
        cell.acceptButton.tag = indexPath.row
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (acceptPressed))  //Tap function will call when user tap on button
        cell.acceptButton.addGestureRecognizer(tapGesture)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let x = assignedJobData?.data?[indexPath.row]
        let vendor = UserDefaults.standard.string(forKey: userdefaultsKey.vendorOptionDriver)?.uppercased()
        if vendor == "OTHER"
        {
            return 227

        }
        
        if x?.status?.uppercased() == "DELIVERED" || x?.status?.uppercased() == "INCOMPLETE"
        {
            return 250

        }
        else
        {
        return 227
        }
    }
    func check(modifyData : String) -> (String , Bool)
    {
        print(modifyData)
        var result = ""
        var resultBool = false

        let formatterForTime = DateFormatter()
        let date = Date()
        formatterForTime.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let passDate = formatterForTime.date(from: modifyData)!
        let currentDateF = formatterForTime.string(from: date as Date)
        let curr = formatterForTime.date(from: currentDateF) // curretn time
        let passPlusGrace = passDate.addingTimeInterval(60*60*24) // 24 hours
        print(formatterForTime.string(from: passPlusGrace))
        print(currentDateF)
       
        
        if curr! < passPlusGrace
        {
            let x = passPlusGrace.timeIntervalSince(curr!)
            let t = x.stringFromTimeInterval()
            result = "  \(t)"
        resultBool = true

        }
        else
        {
            resultBool = false
            result = "For Expenses Contact To Admin".uppercased()
        }
        return (result , resultBool)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

enum jobType {
    case all
    case assigned
    case accepted
}


extension UIImage {
    static func textEmbeded(image: UIImage,
                           string: String,
                isImageBeforeText: Bool,
                          segFont: UIFont? = nil) -> UIImage {
        let font = segFont ?? UIFont.systemFont(ofSize: 16)
        let expectedTextSize = (string as NSString).size(withAttributes: [.font: font])
        let width = expectedTextSize.width + image.size.width + 5
        let height = max(expectedTextSize.height, image.size.width)
        let size = CGSize(width: width, height: height)

        let renderer = UIGraphicsImageRenderer(size: size)
        return renderer.image { context in
            let fontTopPosition: CGFloat = (height - expectedTextSize.height) / 2
            let textOrigin: CGFloat = isImageBeforeText
                ? image.size.width + 5
                : 0
            let textPoint: CGPoint = CGPoint.init(x: textOrigin, y: fontTopPosition)
            string.draw(at: textPoint, withAttributes: [.font: font])
            let alignment: CGFloat = isImageBeforeText
                ? 0
                : expectedTextSize.width + 5
            let rect = CGRect(x: alignment,
                              y: (height - image.size.height) / 2,
                          width: image.size.width,
                         height: image.size.height)
            image.draw(in: rect)
        }
    }
}

extension UITableView {

    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }

    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}

extension UILabel {

    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {

        guard let labelText = self.text else { return }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple

        let attributedString:NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }

        // (Swift 4.2 and above) Line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))


        // (Swift 4.1 and 4.0) Line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))

        self.attributedText = attributedString
    }
}
