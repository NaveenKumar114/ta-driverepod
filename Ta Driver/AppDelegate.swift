//
//  AppDelegate.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-17.
//
import SystemConfiguration
import QuartzCore
import CoreLocation
import UIKit
import CoreData
import UserNotifications
import Messages
import Firebase
import CoreTelephony
import Alamofire
@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    let gcmMessageIDKey = "gcm.message_id"
    var locationManager: CLLocationManager?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        let loggedUsername = UserDefaults.standard.string(forKey: "loggedIn")
            if loggedUsername == "TRUE" {
        setupSignificant()
            }
        Messaging.messaging().delegate = self
       
        if #available(iOS 10.0, *) {
              // For iOS 10 display notification (sent via APNS)
              UNUserNotificationCenter.current().delegate = self

              let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
              UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            } else {
              let settings: UIUserNotificationSettings =
              UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
              application.registerUserNotificationSettings(settings)
            }

            application.registerForRemoteNotifications()
        return true
    }
  

    func applicationWillTerminate(_ application: UIApplication) {
        print("terminate")
        UserDefaults.standard.setValue("TRUE", forKey: "\(userdefaultsKey.appTerminated)")
        let loggedUsername = UserDefaults.standard.string(forKey: "loggedIn")
            if loggedUsername == "TRUE" {
        setupSignificant()
            }
    }
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        Messaging.messaging().token { token, error in
          if let error = error {
            print("Error fetching FCM registration token: \(error)")
          } else if let token = token {
            print("FCM registration token: \(token)")
          //  self.fcmRegTokenMessage.text  = "Remote FCM registration token: \(token)"
          }
        }
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("did \(fcmToken)")
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
      // If you are receiving a notification message while your app is in the background,
      // this callback will not be fired till the user taps on the notification launching the application.
      // TODO: Handle data of notification

      // With swizzling disabled you must let Messaging know about the message, for Analytics
      Messaging.messaging().appDidReceiveMessage(userInfo) // <- this line needs to be uncommented

      // Print message ID.
      if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID: \(messageID)")
      }

      // Print full message.
      print(userInfo)
        
        
        NotificationCenter.default.post(name: Notification.Name("\(notificationNames.updateDashboard)"), object: nil)
print("a")    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      // If you are receiving a notification message while your app is in the background,
      // this callback will not be fired till the user taps on the notification launching the application.
      // TODO: Handle data of notification

      // With swizzling disabled you must let Messaging know about the message, for Analytics
      Messaging.messaging().appDidReceiveMessage(userInfo) // <- this line needs to be uncommented

      // Print message ID.
      if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID: \(messageID)")
      }
        
      // Print full message.
      print(userInfo)
        print("b")
      completionHandler(UIBackgroundFetchResult.newData)
    }
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Housing")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
  
}

extension AppDelegate{

  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              willPresent notification: UNNotification,
    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    let userInfo = notification.request.content.userInfo

    // With swizzling disabled you must let Messaging know about the message, for Analytics
    Messaging.messaging().appDidReceiveMessage(userInfo) // <- this line needs to be uncommented

    // Print message ID.
    if let messageID = userInfo[gcmMessageIDKey] {
      print("Message ID: \(messageID)")
    }

    // Print full message.
    print(userInfo)
    UserDefaults.standard.setValue("FALSE", forKey: "notification")

    // Change this to your preferred presentation option
    completionHandler([[.alert, .sound]])
    print("c")
    NotificationCenter.default.post(name: Notification.Name("\(notificationNames.updateDashboard)"), object: nil)
   
  }

  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              didReceive response: UNNotificationResponse,
                              withCompletionHandler completionHandler: @escaping () -> Void) {
    let userInfo = response.notification.request.content.userInfo
    // Print message ID.
    if let messageID = userInfo[gcmMessageIDKey] {
      print("Message ID: \(messageID)")
    }

    // With swizzling disabled you must let Messaging know about the message, for Analytics
    Messaging.messaging().appDidReceiveMessage(userInfo) // <- this line needs to be uncommented

    // Print full message.
    print(userInfo)
   
    print("d")
    completionHandler()
 /*   UserDefaults.standard.setValue("TRUE", forKey: "notification")
    if let n = UserDefaults.standard.string(forKey: "notification")
    {
        if n == "TRUE"
        {
            print("notification")
          //  notificationRecived()
        }
    }
    NotificationCenter.default.post(name: Notification.Name("\(notificationNames.updateDashboard)"), object: nil) */
  }
}

extension AppDelegate : CLLocationManagerDelegate
{
    
func setupSignificant()
{
    locationManager = CLLocationManager()
      locationManager?.delegate = self
      locationManager?.requestAlwaysAuthorization()
 //   locationManager?.startUpdatingLocation()
    locationManager?.startMonitoringSignificantLocationChanges()
    locationManager?.allowsBackgroundLocationUpdates = true
    let x = UserDefaults.standard.string(forKey: "loc")
    print("fbdhajdah")
    print(x as Any)
    print("dkuahkdhak")
    print("delegate")
    //SwiftLocation.allowsBackgroundLocationUpdates = true
}
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last
        {
            print("New speed is \(location.speed)")
            print("New latitude is \(location.coordinate.latitude)")
            print("New long is \(location.coordinate.longitude)")
            let  lat = String((location.coordinate.latitude))
            let long = String((location.coordinate.longitude))
            let speed = String((location.speed))
            let speedAccur = String((location.speedAccuracy))
            let accur = String((location.horizontalAccuracy))
            prepareForUploadBackground(latitude: lat, longitude: long, speed: speed, speedaccur: speedAccur, accuracy: accur)
            let date = NSDate()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //    formatter.timeZone = TimeZone(abbreviation: "UTC")
            let defaultTimeZoneStr = formatter.string(from: date as Date)
            var x = UserDefaults.standard.string(forKey: "loc")

            UserDefaults.standard.setValue("\(x ?? "") ,\(defaultTimeZoneStr)", forKey: "loc")
             x = UserDefaults.standard.string(forKey: "loc")
            print(x as Any)
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        UserDefaults.standard.setValue("error \(error)", forKey: "loc")
        print(error)

    }
    func prepareForUploadBackground(latitude : String , longitude : String , speed : String , speedaccur : String , accuracy : String)
    {
        UIDevice.current.isBatteryMonitoringEnabled = true
        print(UIDevice.current.isBatteryMonitoringEnabled)
        var level = UIDevice.current.batteryLevel
        print(level*100)
        if level == -1.0
        {
            level = 0.00
        }
        let version = UIDevice.current.systemVersion
        print(version)
        let name = UIDevice.current.name
        print(name)
        let model = UIDevice.current.model
        print(model)
        let state = UIDevice.current.batteryState
        var batteryType = "discharging"
        switch state {
        case .charging:
            print("charging")
        case .full:
            batteryType = "full"
            
        case .unplugged:
            batteryType = "discharging"
            
        default:
            print("else")
        }
        print(getConnectionType())
        let networkInfo = CTTelephonyNetworkInfo()
        let carrier = networkInfo.subscriberCellularProvider
        
        // Get carrier name
        var carrierName = carrier?.carrierName
        if carrierName == nil
        {
            carrierName = ""
        }
       
        _ = UserDefaults.standard.string(forKey: "id")
        print("hhh")
        //let x = jobData!
        let lat = latitude
        let long = longitude
        let speed = speed
        let speedAccur = speedaccur
        let accur = accuracy
        let bearing = ""
        let bearingDegree = ""
     /*   if currentLocation != nil{
            lat = String((currentLocation?.coordinate.latitude)!)
            long = String((currentLocation?.coordinate.longitude)!)
            speed = String((currentLocation?.speed)!)
            speedAccur = String((currentLocation?.speedAccuracy)!)
            accur = String((currentLocation?.horizontalAccuracy)!)
            if previousLocation != nil
            {
                let y = getBearingBetweenTwoPoints1(point1: currentLocation!, point2: previousLocation!)
                bearingDegree = String(y.0)
                bearing = String(y.1)
                print(bearing)
                
            }

        }
        else
        {
            if  locationManager?.location?.coordinate.latitude != nil
            {
                lat = String((locationManager?.location?.coordinate.latitude)!)
                long = String((locationManager?.location?.coordinate.longitude)!)
                speed = String((locationManager?.location?.speed)!)
                speedAccur = String((locationManager?.location?.speedAccuracy)!)
                accur = String((locationManager?.location?.horizontalAccuracy)!)

            }
        }  */
        
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    //    formatter.timeZone = TimeZone(abbreviation: "UTC")
        let defaultTimeZoneStr = formatter.string(from: date as Date)
   
        let defaults = UserDefaults.standard
        
        if UserDefaults.standard.string(forKey: userdefaultsKey.jobSelected) == "true"
        {
        let json: [String: Any] = ["sign_emp_id" : "","sign_emp_name" : "" , "trackid" : "","ta_jobid" : "\(defaults.string(forKey: userdefaultsKey.jobid)!)" , "userid" : "\(defaults.string(forKey: userdefaultsKey.userID)!)" , "ta_form_id" : "\(defaults.string(forKey: userdefaultsKey.taformid)!)" , "clientid" : "\(defaults.string(forKey: userdefaultsKey.cliendid)!)" , "driverid" : "\(defaults.string(forKey: userdefaultsKey.driverID)!)" , "jobstatus" : "\(defaults.string(forKey: userdefaultsKey.jobStatus)!)" , "description" : "" , "trackdatetime" : "\(defaultTimeZoneStr)" , "currentlocation" : "" ,"destinationlocation" : "0,0" , "pickuplocation" : "0,0" , "latitude" : "\(lat)" , "longitude" : "\(long)" , "status" : "active" , "createby" : "\(defaults.string(forKey: userdefaultsKey.name)!)", "modifyby" : "\(defaults.string(forKey: userdefaultsKey.name)!)" , "modifydate" : "\(defaultTimeZoneStr)" ,"deviceid" : "" , "bearing" : "\(bearing)" , "bearingaccuracydegrees" : "\(bearingDegree)" , "speed" : "\(speed)" , "speedaccuracymeterspersecond" : "\(speedAccur)" , "accuracy" : "\(accur)" , "jobnumber" : "\(defaults.string(forKey: userdefaultsKey.jobNumber)!)" , "battery_type" : "\(batteryType)" , "battery_percentage" : "\(Int(level*100))" , "network_type" : "\(getConnectionType())" , "network_operator" : "\(carrierName ?? "")" , "geofence" : "EXIT Warehouse" , "support_driver_flag" : "" ,  "epod_job_id" : "\(defaults.string(forKey: userdefaultsKey.epodJobid)!)" , "main_taform_id" : "\(defaults.string(forKey: userdefaultsKey.mainTaFormID)!)"]
         //   let imageData = fromAttachImage!.jpegData(compressionQuality: 0.50)
            uploadImage(to: URL(string: "\(ConstantsUsedInProject.baseUrl)api/addtrackwithflag")!, params: json)
        print(json)
        }
        else
        {
            let json: [String: Any] = ["sign_emp_id" : "","sign_emp_name" : "" , "trackid" : "","ta_jobid" : "" , "userid" : "" , "ta_form_id" : "" , "clientid" : "0" , "driverid" : "\(defaults.string(forKey: userdefaultsKey.driverID)!)" , "jobstatus" : "IDLE" , "description" : "" , "trackdatetime" : "\(defaultTimeZoneStr)" , "currentlocation" : "" ,"destinationlocation" : "0,0" , "pickuplocation" : "0,0" , "latitude" : "\(lat)" , "longitude" : "\(long)" , "status" : "active" , "createby" : "\(defaults.string(forKey: userdefaultsKey.name)!)", "modifyby" : "\(defaults.string(forKey: userdefaultsKey.name)!)" , "modifydate" : "\(defaultTimeZoneStr)" ,"deviceid" : "" , "bearing" : "\(bearing)" , "bearingaccuracydegrees" : "\(bearingDegree)" , "speed" : "\(speed)" , "speedaccuracymeterspersecond" : "\(speedAccur)" , "accuracy" : "\(accur)" , "jobnumber" : "0" , "battery_type" : "\(batteryType)" , "battery_percentage" : "\(Int(level*100))" , "network_type" : "\(getConnectionType())" , "network_operator" : "\(carrierName ?? "")" , "geofence" : "EXIT Warehouse" , "support_driver_flag" : "" ,  "epod_job_id" : "" , "main_taform_id" : ""]
             //   let imageData = fromAttachImage!.jpegData(compressionQuality: 0.50)

                uploadImage(to: URL(string: "\(ConstantsUsedInProject.baseUrl)api/addtrackwithflag")!, params: json)
        }
        

    }
    func uploadImage( to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                /*  if let data = value.data(using: .utf8) {
                      multipart.append(data, withName: key)
                      print(String(data: data, encoding: String.Encoding.utf8) as Any)

                  } */
                  if let data = value.removingPercentEncoding?.data(using: .utf8) {
                     // do your stuff here
                      multipart.append(data, withName: key)
                      //print(String(data: data, encoding: String.Encoding.utf8) as Any)
                  }
                              }

            //multipart.append(image, withName: "attachmentimage", fileName: "profile", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {

        
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(JobDepartureDeliveryJSON.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                        
                        DispatchQueue.main.async { [self] in
                            
                            if code_str == 200 {
                               print("success")
                          print("appdelegate")
                                
                                
                            }
                            else
                            {
                              
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }

    func getConnectionType() -> String {
        guard let reachability = SCNetworkReachabilityCreateWithName(kCFAllocatorDefault, "www.google.com") else {
            return "NO INTERNET"
        }
        
        var flags = SCNetworkReachabilityFlags()
        SCNetworkReachabilityGetFlags(reachability, &flags)
        
        let isReachable = flags.contains(.reachable)
        let isWWAN = flags.contains(.isWWAN)
        
        if isReachable {
            if isWWAN {
                let networkInfo = CTTelephonyNetworkInfo()
                let carrierType = networkInfo.serviceCurrentRadioAccessTechnology
                
                guard let carrierTypeName = carrierType?.first?.value else {
                    return "UNKNOWN"
                }
                
                switch carrierTypeName {
                case CTRadioAccessTechnologyGPRS, CTRadioAccessTechnologyEdge, CTRadioAccessTechnologyCDMA1x:
                    return "2G"
                case CTRadioAccessTechnologyLTE:
                    return "4G"
                default:
                    return "3G"
                }
            } else {
                return "WIFI"
            }
        } else {
            return "NO INTERNET"
        }
    }

}
