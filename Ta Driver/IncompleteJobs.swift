//
//  IncompleteJobs.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-06-08.
//

import UIKit
import iCarousel
class IncompleteJobs: UIViewController, iCarouselDelegate, iCarouselDataSource {
    @IBOutlet weak var totalPaid: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var carouselView: iCarousel!
    @IBOutlet weak var totalExpense: UILabel!
    @IBOutlet weak var totalComission: UILabel!
    var paid : Double?
    var expense : Double?
    var comissission : Double?
    @IBOutlet weak var deliveredTableView: UITableView!
    var selectedJob = 0
    var delegate : deliveredJobsProtocol?
    @IBOutlet weak var totalCommisisonLabel: UILabel!
    @IBOutlet weak var totalCommissionText: UILabel!
    var fromDate = ""
    var toDate = ""
    var deliveredJobData : AssignedJobJSON?
    var screenSize = UIScreen.main.bounds.size.height
    var screenWidth = UIScreen.main.bounds.size.width
   @IBOutlet weak var driverTextField: UITextField!
   let expiryDatePicker = MonthYearPickerView()
   let toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 400, width: UIScreen.main.bounds.size.width, height: 50))
    var refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "DELIVERED JOBS"
        let vendor = UserDefaults.standard.string(forKey: userdefaultsKey.vendorOptionDriver)?.uppercased()
        if vendor == "OTHER"
        {
            totalCommissionText.text = "Total Rental :"
        }
        carouselView.delegate = self
        carouselView.dataSource = self
        carouselView.type = .rotary
        carouselView.reloadData()
        deliveredTableView.delegate = self
        deliveredTableView.dataSource = self
        deliveredTableView.register(UINib(nibName: "DeliveredTableViewCell", bundle: nil) , forCellReuseIdentifier: "DeliveredTableViewCell")
        deliveredTableView.register(UINib(nibName: "DashboardTableCell", bundle: nil) , forCellReuseIdentifier: "DashboardTableCell")
        let dt = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "yyy-MM"
        let x = formatter2.string(from: dt)
        let calanderDate = Calendar.current.dateComponents([.day], from: dt)
        if calanderDate.day! > 25
        {
            fromDate = x
            let d = dt.adding(months: 1)
            print(formatter2.string(from: d!))
            toDate = formatter2.string(from: d!)
            makePostCallDelivered()
            dateLabel.isUserInteractionEnabled = true
            let gesture2 = UITapGestureRecognizer(target: self, action: #selector(monthYear))
            dateLabel.addGestureRecognizer(gesture2)
            dateLabel.text = "\(fromDate)-26 to \(toDate)-25"     // choose 3 month = 3-26 to 4 - 25
        }
        else
        {
        toDate = x      // choose 3 month = 2-26 to 3- 25
        let d = dt.adding(months: -1)
      //  d.addTimeInterval(60 / 60 / 24 / 30)
        print(formatter2.string(from: d!))
        fromDate = formatter2.string(from: d!)
        makePostCallDelivered()
        dateLabel.isUserInteractionEnabled = true
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(monthYear))
        dateLabel.addGestureRecognizer(gesture2)
        dateLabel.text = "\(fromDate)-26 to \(toDate)-25"
           
        
        }
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
           refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
           deliveredTableView.addSubview(refreshControl)
        deliveredTableView.backgroundView = nil
        deliveredTableView.isOpaque = false
        deliveredTableView.backgroundColor = #colorLiteral(red: 0.9332516193, green: 0.9333856702, blue: 0.9332222342, alpha: 1)
        self.deliveredTableView.tableFooterView = UIView()
        deliveredTableView.separatorStyle = .none
    }
    @objc func refresh(_ sender: AnyObject) {
        refreshControl.endRefreshing()
        makePostCallDelivered()
       // Code to refresh table view
    }
    @objc func monthYear()
    {
        deliveredTableView.reloadData()
        let month = Calendar.current.component(.month, from: Date())
        _ = Calendar.current.component(.year, from: Date())
        var m = "\(month)"
        if m.count == 1
        {
            print("1")
            m = "0\(month)"
        }
       // historyDate = "\(year)-\(m)"
        expiryDatePicker.backgroundColor = UIColor.white
           expiryDatePicker.setValue(UIColor.black, forKey: "textColor")
           expiryDatePicker.autoresizingMask = .flexibleWidth
           expiryDatePicker.contentMode = .center
        expiryDatePicker.frame = CGRect.init(x: 0.0, y: self.screenSize - 400, width: UIScreen.main.bounds.size.width, height: 350)
           self.view.addSubview(expiryDatePicker)

           toolBar.barStyle = .default
           toolBar.isTranslucent = true
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.actionPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)

           toolBar.setItems([spaceButton, doneButton], animated: false)
           toolBar.isUserInteractionEnabled = true
           self.view.addSubview(toolBar)

           //DISABLE RIGHT ITEM & LEFT ITEM
          //        disableCancelAndSaveItems()

           //DISABLED SELECTION FOR ALL CELLS
          //        ableToSelectCellsAndButtons(isAble: false)

           //DISABLE RIGHT ITEM & LEFT ITEM
          // isEnableCancelAndSaveItems(isEnabled: false)

           //SHOW GREY BACK GROUND
          // showGreyOutView(isShow: true)
    
        expiryDatePicker.onDateSelected = { [self] (month: Int, year: Int) in
              // self.expiredDetailOutlet.text = string
            var m = "\(month)"
            if m.count == 1
            {
                print(month , year)
                m = "0\(month)"
            }

            //let formatter = DateFormatter()
            //formatter.dateFormat = "dd-MM-yyyy"
            let formatter2 = DateFormatter()
            formatter2.dateFormat = "yyyy-MM"
            let z = formatter2.date(from: "\(year)-\(m)")
            let x = formatter2.string(from: z!)
            toDate = x
            let d = z!.adding(months: -1)
          //  d.addTimeInterval(60 / 60 / 24 / 30)
            print(formatter2.string(from: d!))
            fromDate = formatter2.string(from: d!)
            dateLabel.text = "\(fromDate)-26 to \(toDate)-25"

          //  self.historyDate = "\(year)-\(m)"

               
           }
        
    }
    @objc func actionPicker() {
        expiryDatePicker.removeFromSuperview()
        toolBar.removeFromSuperview()
        view.endEditing(true)
       makePostCallDelivered()
        
    }

    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let frame  = CGRect(x: 0, y: 0, width: 330, height: 450)
        
        let x = deliveredJobData?.data?[index]
        
        let myView = DeliveredList.instantiate(message: "\(index)")
        myView.frame = frame
        myView.x()
        let y = Double(x?.overnightTotalAmount ?? "0.0")
        let s = String(format: "%.2f", y ?? 0.0)
        myView.advance.text = "MYR \(s)"
        myView.date.text = convertDateFormater((x?.jobDate)!)
        myView.comission.text = "MYR \(x?.jobCommision ?? "")"
        myView.taNumber.text = x?.taTadynamicNumber
        myView.from.text = x?.jobFrom?.uppercased()
        myView.to.text = x?.jobClientName?.uppercased()
        myView.jobNumbwe.text = x?.jobNumber
        //myView.frame = frame
        myView.acceptButton.tag = index
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (expensePressed(sender:)))
        myView.expenseListButton.addGestureRecognizer(tapGesture)
        myView.expenseListButton.tag = index
        myView.addExpenseButton.tag = index
        let addGesture = UITapGestureRecognizer(target: self, action: #selector (addExpense(sender:)))
        let trackGesture = UITapGestureRecognizer(target: self, action: #selector (trackjob(sender:)))
        myView.acceptButton.addGestureRecognizer(trackGesture)
        myView.addExpenseButton.addGestureRecognizer(addGesture)
        myView.segmentCOntrol.tag = index
        myView.segmentCOntrol.addTarget(self, action: #selector(segmentAction(_:)), for: .valueChanged)
        print("selceted")
        myView.expense.text = "MYR \(x?.sumOfCashExpense ?? "")"
        myView.jobNmae.text = x?.jobStatus?.uppercased()
        return myView
    }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  dateFormatter.string(from: date!)

        }
    func numberOfItems(in carousel: iCarousel) -> Int {
        return deliveredJobData?.data?.count ?? 0
    }
    @objc func addExpense(sender: UITapGestureRecognizer)
    {
        let i = sender.view?.tag

        selectedJob = i!

        //performSegue(withIdentifier: "deliverToAddExpense", sender: nil)
        let x = deliveredJobData?.data?[selectedJob]
        delegate?.addExpense(data: x!)
    }
    @objc func trackjob(sender: UITapGestureRecognizer)
    {
        let i = sender.view?.tag

        selectedJob = i!
        let x = deliveredJobData?.data?[selectedJob]
        delegate?.toTrack(data: x!)
      //  performSegue(withIdentifier: "toTrack", sender: nil)
    }
    @objc func segmentAction(_ segmentedControl: UISegmentedControl) {
        let i = segmentedControl.tag
           switch (segmentedControl.selectedSegmentIndex) {
           case 0:
            let c = carouselView.currentItemView as! DeliveredList
            c.from.text = deliveredJobData?.data?[i].jobFrom?.uppercased()
            print("1")
               break // Uno
           case 1:
            let c = carouselView.currentItemView as! DeliveredList
            c.from.text = deliveredJobData?.data?[i].jobTo?.uppercased()
            print("2")
               break // Dos
           case 2:
               break // Tres
           default:
               break
           }
       }
    @objc func expensePressed(sender: UITapGestureRecognizer)
    {
        let i = sender.view?.tag

        selectedJob = i!
        let x = deliveredJobData?.data?[selectedJob]
        delegate?.toExpenses(data: x!)
      //  performSegue(withIdentifier: "toExpense", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toExpense"
        {
            let x = deliveredJobData?.data?[selectedJob]

            let vc = segue.destination as! Expenses
            vc.jobData = x
        }
        if segue.identifier == "toTrack"
        {
            let x = deliveredJobData?.data?[selectedJob]

            let vc = segue.destination as! TrackJobDelivered
            vc.jobData = x
        }
        if segue.identifier == "deliverToAddExpense"
        {
            let x = deliveredJobData?.data?[selectedJob]

            let vc = segue.destination as! AddExpense
            vc.jobData = x
        }
        
    }

    func makePostCallDelivered() {
      
       
        let id = UserDefaults.standard.string(forKey: "id")
        let decoder = JSONDecoder()
       
        let postString = "id=\(id!)&status=INCOMPLETE&from=\(fromDate)-26&to=\(toDate)-25"
        // create post request
        print(postString)
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)job/driverjobbydates")
        {
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = postString.data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                  if  let loginBaseResponse = try? decoder.decode(AssignedJobJSON.self, from: data!)
                  {
                    let code_str = loginBaseResponse.code
                     print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    print("delivered")
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            totalCommisisonLabel.text = "MYR \(loginBaseResponse.bayar?.pay ?? "0.0")"
                            deliveredJobData = nil
                            print(loginBaseResponse as Any)
                            deliveredJobData = loginBaseResponse
                            carouselView.reloadData()
                            deliveredTableView.reloadData()
                            
                        }else if code_str == 201  {
                            totalCommisisonLabel.text = "MYR \(loginBaseResponse.bayar?.pay ?? "0.0")"
                            deliveredJobData = nil
                            print(loginBaseResponse as Any)
                            deliveredJobData = loginBaseResponse
                           // carouselView.reloadData()
                            deliveredTableView.reloadData()
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            print(loginBaseResponse as Any)

                            let alert = UIAlertController(title: "DriverJob", message: "\(loginBaseResponse.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                           // self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                }
                    
                    
                }
            }
            task.resume()
        }
    }
}

extension IncompleteJobs : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if deliveredJobData?.data?.count ?? 0 == 0 {
              self.deliveredTableView.setEmptyMessage("No Job Found")
          } else {
            self.deliveredTableView.restore()
          }

        return deliveredJobData?.data?.count ?? 0
      //  return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let x = deliveredJobData?.data?[indexPath.row]

        let cell = deliveredTableView.dequeueReusableCell(withIdentifier: "DeliveredTableViewCell") as! DeliveredTableViewCell
        let vendor = UserDefaults.standard.string(forKey: userdefaultsKey.vendorOptionDriver)?.uppercased()
        if vendor == "OTHER"
        {
            let cell = deliveredTableView.dequeueReusableCell(withIdentifier: "DashboardTableCell") as! DashboardTableCell
            
            let y = Double(x?.jobCommision ?? "0.0")
            let s = String(format: "%.2f", y ?? 0.0)
            cell.payToDriver.text = "MYR \(s)"
            cell.date.text = convertDateFormater((x?.jobDate!)!)
            cell.commission.text = ""
            cell.commissionText.text = "Rental"
            cell.payToDriver.textColor = ConstantsUsedInProject.appThemeColor

            cell.payToDriverText.text = ""
            cell.taNumber.text = x?.taTadynamicNumber
            cell.from.text = x?.shipperName?.uppercased()
            cell.to.text = x?.consigneeName?.uppercased()
            cell.jobNuber.text = x?.jobNumber
            cell.truckType.text = x?.jobChargeType
            cell.truckNumber.text = x?.truckNumber
          //  cell.companyName.text = x?.jobClientName
            cell.fromAddress.text = x?.shipperLocationaddress
            cell.toAddress.text = x?.consigneeLocationaddress
            cell.status.text = x?.status
            if x?.status?.uppercased() == "INCOMPLETE"
                {
                cell.buttonLabel.text = "TRACK"
                cell.status.textColor = .darkGray
            
                cell.buttonView.backgroundColor = .systemGray

            }
            else
            {
                cell.buttonLabel.text = "TRACK"

                cell.status.textColor = .systemGreen
                cell.buttonView.backgroundColor = .systemGreen
            }
            cell.acceptButton.tag = indexPath.row
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector (trackjob(sender:)))
            cell.acceptButton.addGestureRecognizer(tapGesture)
            return cell
        }
        let y = Double(x?.totalPaytodriver ?? "0.0")
        let s = String(format: "%.2f", y ?? 0.0)
        cell.payToDriver.text = "MYR \(s)"
        cell.date.text = convertDateFormater((x?.jobDate!)!)
        cell.commission.text = "MYR \(x?.jobCommision ?? "")"
        cell.taNumber.text = x?.taTadynamicNumber
       
        cell.jobNuber.text = x?.jobNumber
        cell.truckType.text = x?.jobChargeType
        cell.truckNumber.text = x?.truckNumber
        //cell.companyName.text = x?.jobClientName
        cell.trackButton.tag = indexPath.row
        cell.edpenseBUtton.tag = indexPath.row
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (expensePressed(sender:)))
        cell.edpenseBUtton.addGestureRecognizer(tapGesture)
        cell.edpenseBUtton.setBackgroundColor(#colorLiteral(red: 0.1450980392, green: 0.4941176471, blue: 0.1725490196, alpha: 1), forState: .highlighted)

        //myView.expenseListButton.tag = index
        //myView.addExpenseButton.tag = index
        //let addGesture = UITapGestureRecognizer(target: self, action: #selector (addExpense(sender:)))
        let trackGesture = UITapGestureRecognizer(target: self, action: #selector (trackjob(sender:)))
        cell.trackButton.addGestureRecognizer(trackGesture)
        cell.trackButton.setBackgroundColor(#colorLiteral(red: 0.1450980392, green: 0.4941176471, blue: 0.1725490196, alpha: 1), forState: .highlighted)

        cell.from.text = x?.shipperName?.uppercased()
        cell.to.text = x?.consigneeName?.uppercased()
        cell.fromAddress.text = x?.shipperLocationaddress
        cell.toAddress.text = x?.consigneeLocationaddress
        cell.status.text = x?.status
        cell.status.textColor = .darkGray
        cell.buttonView.backgroundColor = .systemGray
        cell.buttonView2.backgroundColor = .systemGray
        let c = check(modifyData: x!.modifydate!)
        cell.timeLeftLabel.text =  c.0
        if x?.operationStatus?.uppercased() == "VERIFIED" || c.1 == false
        {
            cell.timeLeftLabel.text = "For Expenses Contact To Admin".uppercased()
            cell.timeLeftLabel.textColor = .systemRed
        }
        else
        {
            cell.timeLeftLabel.textColor = .systemGreen

        }
        //myView.addExpenseButton.addGestureRecognizer(addGesture)
        return cell
    }
    func check(modifyData : String) -> (String , Bool)
    {
        print(modifyData)
        var result = ""
        var resultBool = false

        let formatterForTime = DateFormatter()
        let date = Date()
        formatterForTime.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let passDate = formatterForTime.date(from: modifyData)!
        let currentDateF = formatterForTime.string(from: date as Date)
        let curr = formatterForTime.date(from: currentDateF) // curretn time
        let passPlusGrace = passDate.addingTimeInterval(60*60*24) // 24 hours
        print(formatterForTime.string(from: passPlusGrace))
        print(currentDateF)
       
        
        if curr! < passPlusGrace
        {
            let x = passPlusGrace.timeIntervalSince(curr!)
            let t = x.stringFromTimeInterval()
            result = "  \(t)"
        resultBool = true

        }
        else
        {
            resultBool = false
            result = "For Expenses Contact To Admin".uppercased()
        }
        return (result , resultBool)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let vendor = UserDefaults.standard.string(forKey: userdefaultsKey.vendorOptionDriver)?.uppercased()
        if vendor == "OTHER"
        {
            return 227

        }
        return 250
    }
}
