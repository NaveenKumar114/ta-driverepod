//
//  EnterOtp.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-29.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseAuth
class EnterOtp: UIViewController {
    @IBOutlet weak var otpTextField: UITextField!
    var tokenID = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        Messaging.messaging().token { token, error in
          if let error = error {
            print("Error fetching FCM registration token: \(error)")
          } else if let token = token {
            print("FCM registration token: \(token)")
            self.tokenID = token
          //  self.fcmRegTokenMessage.text  = "Remote FCM registration token: \(token)"
          }
        }
        // Do any additional setup after loading the view.
    }
    

    @IBAction func confirmButton(_ sender: Any) {
        if otpTextField.text != nil
        {
            verifyOtp()
        }
    }
   
    func verifyOtp()
    {
        let verificationCode = otpTextField.text!
        let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID!,
            verificationCode: verificationCode)
        Auth.auth().signIn(with: credential) { [self] (authResult, error) in
          if let error = error {
            let authError = error as NSError
            print(authError.description)
            return
          }

            print("Corrects")
          // User has signed in successfully and currentUser object is valid
            _ = Auth.auth().currentUser
            makePostCall()
        }
    }
    func makePostCall() {
        let phone = UserDefaults.standard.string(forKey: "mobile")
        let decoder = JSONDecoder()
       
        let postString = "driver_mobile=\(phone!)&tokenid=\(tokenID)"
        print(postString)
        // create post request
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)driver/iosdriverloginbymobile")
        {
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = postString.data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                     print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                    let loginBaseResponse = try? decoder.decode(EmailJson.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            let defaults = UserDefaults.standard
                            defaults.setValue(loginBaseResponse?.driver?.driverIDPrimary ?? "", forKey: "id") // this is the driver id used
                            defaults.setValue(loginBaseResponse?.driver?.driverName ?? "", forKey: "name")
                            defaults.setValue(loginBaseResponse?.driver?.driverLicenseFile ?? "", forKey: "licenceimg")
                            defaults.setValue(loginBaseResponse?.driver?.profileImage ?? "", forKey: "img")

                            defaults.setValue(loginBaseResponse?.driver?.driverAddress ?? "", forKey: "address")
                            defaults.setValue(loginBaseResponse?.driver?.driverMobile ?? "", forKey: "mobile")
                            defaults.setValue(loginBaseResponse?.driver?.vendorOptiondriver ?? "", forKey: "\(userdefaultsKey.vendorOptionDriver)")

                            defaults.setValue("false", forKey: "\(userdefaultsKey.jobSelected)")
                            defaults.setValue("TRUE", forKey: "loggedIn")

                            print(loginBaseResponse as Any)
                            let storyboard2 = UIStoryboard(name: "Dashboard", bundle: nil)

                            let dashboard = storyboard2.instantiateViewController(identifier: "dash")

                            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(dashboard)
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Login", message: "Invalid Email & Password", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

}
