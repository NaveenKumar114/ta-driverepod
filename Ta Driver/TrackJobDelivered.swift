//
//  TrackJobDelivered.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-28.
//

import UIKit
import MapKit
class TrackJobDelivered: UIViewController , UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var to: UILabel!
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var commission: UILabel!
    @IBOutlet weak var viewWithImage: UIView!
    @IBOutlet weak var fromAddress: UILabel!
    @IBOutlet weak var toAddress: UILabel!
    @IBOutlet weak var payToDriver: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var commissionTExt: UILabel!
    @IBOutlet weak var payToDriverTExt: UILabel!
    @IBOutlet weak var date: UILabel!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var x = 0
        if trackData != nil
        {
            x = trackData?.trackList?.count ?? 0
            x = x + 1
            print(x)

        }
       
        return x
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == trackData?.trackList?.count
        {
            let cell = tackTableView.dequeueReusableCell(withIdentifier: "TrackJobImages") as! TrackJobImages
            _ = (trackData?.trackList!.count)!
            if let x = trackData?.trackList?[0]
            {
                cell.id.text = x.signEmpID ?? ""
                cell.name.text = x.signEmpName ?? ""
                
                if x.signimage != nil
                {
                    let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)track/\(x.signimage!)").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                    print(urlStr as Any)
                    let url = URL(string: urlStr!)
                    
                    DispatchQueue.global().async {
                        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                        DispatchQueue.main.async { [self] in
                            if data != nil
                            {
                            cell.signature.image = UIImage(data: data!)
                            }
                           // logoImageVIew.image = UIImage(data: data!)
                          //  logoImageVIew.contentMode = .scaleAspectFit
                            
                          
                        }
                    }
                }
                for i in trackData!.trackList!
                {
                    if i.jobstatus! == "PICKED UP"
                    {
                        if x.attachmentimage != nil
                        {
                            let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)track/\(i.attachmentimage!)").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                            print(urlStr as Any)
                            let url = URL(string: urlStr!)
                            
                            DispatchQueue.global().async {
                             if   let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                             {
                                DispatchQueue.main.async { [self] in
                                    
                                    cell.PICKUP.image = UIImage(data: data)
                                   // logoImageVIew.image = UIImage(data: data!)
                                  //  logoImageVIew.contentMode = .scaleAspectFit
                                }
                                  
                                }
                            }
                        }

                    }
                }
                if x.attachmentimage != nil
                {
                    let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)track/\(x.attachmentimage!)").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                    print(urlStr as Any)
                    let url = URL(string: urlStr!)
                    
                    DispatchQueue.global().async {
                       if let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                       {
                        DispatchQueue.main.async { [self] in
                            cell.delivery.image = UIImage(data: data)
                           // logoImageVIew.image = UIImage(data: data!)
                          //  logoImageVIew.contentMode = .scaleAspectFit
                            
                        }
                        }
                    }
                }
            }
            return cell
        }
        else
        {
        let cell = tackTableView.dequeueReusableCell(withIdentifier: "JobListCell") as! JobListCell
        let i = ((trackData?.trackList!.count)! - 1) - indexPath.row
        if let x = trackData?.trackList?[i]
        {
            print(x.jobstatus as Any)
            print(x.currentlocation as Any)
            print(x.signimage as Any)
           // cell.address.text = x.pickuplocation ?? ""
            let d = convertDateFormater(x.trackdatetime!)
            cell.date.text = d.0
            cell.time.text = d.1

            cell.status.text = x.jobstatus ?? ""
            cell.circleVIew.layer.cornerRadius = cell.circleVIew.frame.width / 2
           
            switch x.jobstatus!  {
            case "ASSIGNED":
               // cell.iconImage.image = UIImage(named: "")?.withTintColor(.white)
            print("ee")
            case "COLLECTING":
                cell.iconImage.image = UIImage(named: "black_collecting")?.withTintColor(.white)
                cell.circleVIew.applyGradient(color: .blue)

            case "PICKED UP":
                cell.iconImage.image = UIImage(named: "black_pickedup2")?.withTintColor(.white)
                cell.circleVIew.applyGradient(color: .red)

            case "DEPARTURE":
                cell.iconImage.image = UIImage(named: "black_departure")?.withTintColor(.white)
                cell.circleVIew.applyGradient(color: .systemBlue)

            case "IN TRANSIT":
                cell.iconImage.image = UIImage(named: "black_transit")?.withTintColor(.white)
                cell.circleVIew.applyGradient(color: .black)

            case "DELIVERED":
                cell.iconImage.image = UIImage(named: "black_delivered")?.withTintColor(.white)
                cell.circleVIew.applyGradient(color: .green)
            case "PICKUP ARRANGED":
                cell.iconImage.image = UIImage(named: "black_pickup_arranged")?.withTintColor(.white)
                cell.circleVIew.applyGradient(color: .yellow)
            default:
                cell.iconImage.image = UIImage(named: "black_pickedup")?.withTintColor(.white)
                cell.circleVIew.applyGradient(color: .yellow)

                
            }
            if x.currentlocation != nil || x.currentlocation != ""
            {
              
                cell.address.text = x.currentlocation ?? ""
            }
            else
            {
            if x.latitude != nil && x.longitude != nil && x.latitude != "" && x.longitude != ""
            {
                let lat : CLLocationDegrees = Double(x.latitude!)!
                let long : CLLocationDegrees = Double(x.longitude!)!

            let cityCoords = CLLocation(latitude: lat, longitude: long)
                getAdressName(coords: cityCoords, label: cell.address)
            }
            }
            
                cell.driverName.text = x.drivername ?? ""
            
            
        }
        return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == trackData?.trackList?.count

        {
         return 100
        }
        else
        {
        return 70
        }
    }
    func convertDateFormater(_ date: String) -> (String , String)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "h:mm a"
            let t = dateFormatter.string(from: date!)

            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  (dateFormatter.string(from: date!) , t)

        }
    func convertDateFormaterDate(_ date: String) -> (String)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
        //dateFormatter.dateFormat = "h:mm a"
            //let t = dateFormatter.string(from: date!)

            dateFormatter.dateFormat = "dd-MMM-yyyy"
            return  (dateFormatter.string(from: date!))

        }
    var jobData : Datum?
    var trackData : TrackListJSON?
    @IBOutlet weak var tackTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
makeGetCall()
        viewWithImage.layer.borderWidth = 1
        viewWithImage.layer.borderColor = UIColor.white.cgColor
        viewWithImage.layer.cornerRadius = viewWithImage.frame.height / 2
        let x = jobData
        tackTableView.delegate = self
        tackTableView.dataSource = self
        tackTableView.register(UINib(nibName: "JobListCell", bundle: nil) , forCellReuseIdentifier: "JobListCell")
        
        tackTableView.register(UINib(nibName: "TrackJobImages", bundle: nil) , forCellReuseIdentifier: "TrackJobImages")

        let y = Double(x?.totalPaytodriver ?? "0.0")
        let s = String(format: "%.2f", y ?? 0.0)
       payToDriver.text = "MYR \(s)"
        commission.text = "MYR \(x?.jobCommision ?? "")"
        from.text = x?.shipperName?.uppercased()
        to.text = x?.consigneeName?.uppercased()
        fromAddress.text = x?.shipperLocationaddress
        toAddress.text = x?.consigneeLocationaddress
        status.text = jobData?.jobStatus ?? ""
        date.text = convertDateFormaterDate((x?.jobDate!)!)
        //client.text = jobData?.jobClientName?.uppercased() ?? ""
        self.title = jobData?.jobNumber ?? ""
        self.tackTableView.tableFooterView = UIView()
        let vendor = UserDefaults.standard.string(forKey: userdefaultsKey.vendorOptionDriver)?.uppercased()
        if vendor == "OTHER"
        {
            payToDriverTExt.isHidden = true
            payToDriver.isHidden = true
            commissionTExt.text = "Rental"
            let y = Double(x?.jobCommision ?? "0.0")
            let s = String(format: "%.2f", y ?? 0.0)
            commission.text = "MYR \(s)"
            
        }
        // Do any additional setup after loading the view.
    }
    
    func makeGetCall() {
        //TAFORMID TADRIVERID 
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(ConstantsUsedInProject.baseUrl)track/index_get/\(jobData!.taFormID!)/\(jobData!.taDriverID!)/2021-04-26")! as URL)
        request.httpMethod = "GET"
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            do {
                print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                let responseJ = try? decoder.decode(TrackListJSON.self, from: data!)
                let code_str = responseJ?.code
                DispatchQueue.main.async {
                    if code_str == 200 {
                        print("success")
                        print(responseJ as Any)
                        self.trackData = responseJ
                        self.tackTableView.reloadData()
                    }else if code_str == 201  {
                        let alert = UIAlertController(title: "Track", message: "\(responseJ?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        task.resume()
    }
    func getAdressName(coords: CLLocation , label : UILabel) {

        CLGeocoder().reverseGeocodeLocation(coords) { (placemark, error) in
                if error != nil {
                    print("Hay un error")
                } else {

                    let place = placemark! as [CLPlacemark]
                    if place.count > 0 {
                        let place = placemark![0]
                        var adressString : String = ""
                        if place.thoroughfare != nil {
                            adressString = adressString + place.thoroughfare! + ", "
                        }
                        if place.subThoroughfare != nil {
                            adressString = adressString + place.subThoroughfare! + "\n"
                        }
                        if place.locality != nil {
                            adressString = adressString + place.locality! + " - "
                        }
                        if place.postalCode != nil {
                            adressString = adressString + place.postalCode! + "\n"
                        }
                        if place.subAdministrativeArea != nil {
                            adressString = adressString + place.subAdministrativeArea! + " - "
                        }
                        if place.country != nil {
                            adressString = adressString + place.country!
                        }
                        
                        label.text = adressString
                    }
                }
            }
      }


}
