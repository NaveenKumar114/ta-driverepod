//
//  DeleteExpenseJson.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-06-07.
//

import Foundation
struct DeleteExpenseJSON: Codable {
    let code: Int?
    let response: String?
}
