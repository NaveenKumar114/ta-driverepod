//
//  ExpenseTypeJson.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-06-06.
//

import Foundation
import Foundation

// MARK: - ExpenseTypeJSON
struct ExpenseTypeJSON: Codable {
    let code: Int?
    let response: String?
    let advances: [Advance]?
}

// MARK: - Advance
struct Advance: Codable {
    let advanceID, advanceName, createdDate: String?
    let createdBy: CreatedBy?
    let createdID, updatedDate, updatedBy: String?

    enum CodingKeys: String, CodingKey {
        case advanceID = "advance_id"
        case advanceName = "advance_name"
        case createdDate = "created_date"
        case createdBy = "created_by"
        case createdID = "created_id"
        case updatedDate = "updated_date"
        case updatedBy = "updated_by"
    }
}

enum CreatedBy: String, Codable {
    case admin = "Admin"
    case muhun = "Muhun"
}
