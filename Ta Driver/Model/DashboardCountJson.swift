//
//  DashboardCountJson.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-06-01.
//

import Foundation
// MARK: - DashboardCountJSON
struct DashboardCountJSON: Codable {
    let code: Int?
    let response: String?
    let dashboard: countData?
}

// MARK: - Dashboard
struct countData: Codable {
    let assigned, accepted, delivery, total , incomplte: String?
}
