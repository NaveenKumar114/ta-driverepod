//
//  AddExpensesJson.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-28.
//

import Foundation


// MARK: - AddExpensesJSON
struct AddExpensesJSON: Codable {
    let header: Header?
}

// MARK: - Header
struct Header: Codable {
    let status : Int?
    let response , expenseid: String?
    let loginDetail: LoginDetail?

    enum CodingKeys: String, CodingKey {
        case status, expenseid, response
        case loginDetail = "login_detail"
    }
}

// MARK: - LoginDetail
struct LoginDetail: Codable {
    let chargeType, chargeTypeID, truckNumber, truckNumberID: String?
    let driverVendorID, driverVendorType, userid, jobid: String?
    let jobtruckid, driverid, drivername, username: String?
    let loginDetailDescription, expenseamount, typeofexpense, modeofpayment: String?
    let createdby, modifyby, modifydate, deviceid: String?
    let attachement1: String?

    enum CodingKeys: String, CodingKey {
        case chargeType = "charge_type"
        case chargeTypeID = "charge_type_id"
        case truckNumber = "truck_number"
        case truckNumberID = "truck_number_id"
        case driverVendorID = "driver_vendor_id"
        case driverVendorType = "driver_vendor_type"
        case userid, jobid, jobtruckid, driverid, drivername, username
        case loginDetailDescription = "description"
        case expenseamount, typeofexpense, modeofpayment, createdby, modifyby, modifydate, deviceid, attachement1
    }
}
