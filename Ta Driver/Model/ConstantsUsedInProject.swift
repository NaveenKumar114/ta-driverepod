//
//  ConstantsUsedInProject.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-23.
//

import Foundation
import UIKit
struct ConstantsUsedInProject {
    static let appThemeColor : UIColor = #colorLiteral(red: 0.9219180346, green: 0.3579308987, blue: 0.2009037137, alpha: 1)
    static let appThemeColorName = "AccentColor"
    static let baseUrl = "https://thefollo.com/epod_demo/taformapi/driverapi/api/"
    static let baseImgUrl = "https://thefollo.com/epod_demo/uploads/"
    //    static let baseUrl = "https://thefollo.com/epod_demo/taformapi/driverapi/api/"
  //  static let baseUrl = "https://thefollo.com/epod_live/taformapi/driverapi/api/"


}
struct userdefaultsKey {
    static let jobid = "jobid"
    static let userID = "userid"
   // static let truckid = "truckid" //to_form_id
    static let cliendid = "clientid"
    static let driverID = "id" // driver id is id got during login
    static let jobStatus = "jobstatus"
    static let createBy = "createBy"
    static let name = "name"
    static let jobNumber = "jobnumer"
    static let jobSelected = "jobselected"
    static let epodJobid = "epodjobid"
    static let taformid = "taformid"
    static let mainTaFormID = "maintaformid"
    static let currentAddress = "currentAddress"
    static let appTerminated = "appTerminated"
    static let vendorOptionDriver = "vendoroptiondriver"
}

struct notificationNames {
    static let updateDashboard = "updatedashboard"
}
