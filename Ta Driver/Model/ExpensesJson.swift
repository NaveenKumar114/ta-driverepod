//
//  ExpensesJson.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-27.
//

import Foundation
// MARK: - ExpensesJSON
struct ExpensesJSON: Codable {
    let code: Int?
     let response: String?
     let expense: [Expense]?
     let expensereport: Expensereport?
 }

 // MARK: - Expense
 struct Expense: Codable {
     let expenseID, taFormID, epodJobID, loginID: String?
     let expenseCash, expenseCard, expenseRemark, expenseStatus: String?
     let expenseImage, expenseType, expenseTypeID, expenseCreatedDate: String?
     let expenseModifyDate, expenseCreateBy, expenseModifyBy: String?

     enum CodingKeys: String, CodingKey {
         case expenseID = "expense_id"
         case taFormID = "ta_form_id"
         case epodJobID = "epod_job_id"
         case loginID = "login_id"
         case expenseCash = "expense_cash"
         case expenseCard = "expense_card"
         case expenseRemark = "expense_remark"
         case expenseStatus = "expense_status"
         case expenseImage = "expense_image"
         case expenseType = "expense_type"
         case expenseTypeID = "expense_type_id"
         case expenseCreatedDate = "expense_created_date"
         case expenseModifyDate = "expense_modify_date"
         case expenseCreateBy = "expense_create_by"
         case expenseModifyBy = "expense_modify_by"
     }
 }

 // MARK: - Expensereport
 struct Expensereport: Codable {
     let totalPaytodriver, sumOfCashExpense, sumOfCardExpense, totalGivenbydriver: String?
     let totalAdvance, totalCommision, totalBalance: String?
    let operationStatus : String?

     enum CodingKeys: String, CodingKey {
        case operationStatus = "operation_status"
         case totalPaytodriver = "total_paytodriver"
         case sumOfCashExpense = "sum_of_cash_expense"
         case sumOfCardExpense = "sum_of_card_expense"
         case totalGivenbydriver = "total_givenbydriver"
         case totalAdvance = "total_advance"
         case totalCommision = "total_commision"
         case totalBalance = "total_balance"
     }
 }
