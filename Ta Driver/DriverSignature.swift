//
//  DriverSignature.swift
//  Ta Driver
//
//  Created by Naveen Natrajan on 2021-04-21.
//

import UIKit

class DriverSignature: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var empName: UITextField!
    @IBOutlet weak var empId: UITextField!
    @IBOutlet weak var signatureView: Canvas!
    var delegate : signatureDelegate?
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
     
        return false

    }
    override func viewDidLoad() {
            super.viewDidLoad()
        empId.delegate = self
        empName.delegate = self
        signatureView.isUserInteractionEnabled = true

            setupViews()

            signatureView.setStrokeColor(color: .black)
        }

        func setupViews(){

            signatureView.layer.borderWidth = 0.5
            signatureView.layer.borderColor = UIColor.black.cgColor
            signatureView.layer.cornerRadius = 10
        }

        @IBAction func clearBtnTapped(_ sender: UIButton) {

            signatureView.clear()
        }
    @IBAction func saveButton(_ sender: Any) {
        if empName.text == "" || empId.text == ""
        {
            let alert = UIAlertController(title: "Signature", message: "Please enter all values", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
        let image = signatureView.asImage()
        delegate?.signDone(signImage: image, empName: empName.text!, empId: empId.text!)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}
protocol signatureDelegate {
    func signDone(signImage : UIImage , empName : String , empId : String)
}
extension UIView {

    // Using a function since `var image` might conflict with an existing variable
    // (like on `UIImageView`)
    func asImage() -> UIImage {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: bounds)
            return renderer.image { rendererContext in
                layer.render(in: rendererContext.cgContext)
            }
        } else {
            UIGraphicsBeginImageContext(self.frame.size)
            self.layer.render(in:UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return UIImage(cgImage: image!.cgImage!)
        }
    }
}
